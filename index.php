<?php

ob_start();

session_start();

$locale = 'en';

require 'autoload.php';
require 'database.php';
require 'translations.php';

?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8"/>
    <title>Volunteer management system</title>

    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap-theme.min.css">
    <link rel="stylesheet" type="text/css" href="css/datepicker.min.css"/>
    <link rel="stylesheet" type="text/css" href="css/style.css"/>

    <link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">
    <link rel="icon" type="image/x-icon" href="img/favicon.ico">

    <script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/datepicker.min.js"></script>
    <script type="text/javascript" src="js/functions.js"></script>
  </head>
  <body>
<?php
require 'content.php';
?>
    <div class="footer">
<?php
require 'footer.php';
?>
    </div>
  </body>
</html>
