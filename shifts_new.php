<?php

if (isset($_GET["event_id"])) {
  $event_id = $_GET["event_id"];
}

if ($event_id && (!isset($_SESSION['user_id']) || !(isset($_SESSION['user_type']) && $_SESSION['user_type'] == 1))) {
  header("Location: index.php");
  die();
}

$end_before_start_error = false;

if (isset($_POST["submit"])) {
  $new_shift = [];
  $new_shift['date'] = clean($_POST["date"]);
  $new_shift['start_time'] = clean($_POST["start_time"]);
  $new_shift['end_time'] = clean($_POST["end_time"]);
  $new_shift['people_needed'] = clean($_POST["people_needed"]);
  $new_shift['event_id'] = $event_id;
  $shift_start = strtotime("{$new_shift['date']} {$new_shift['start_time']}");
  $shift_end = strtotime("{$new_shift['date']} {$new_shift['end_time']}");
  $shift_duration = $shift_end - $shift_start;
  if ($shift_duration < 0) {
    $end_before_start_error = true;
  } elseif (Shifts::create($new_shift)) {
    LogEntries::create("[new_shift] User '{$_SESSION['username']}' added a new shift to event (id = {$event_id}).");
    header("Location: index.php?content=shifts_index&event_id={$event_id}");
    die();
  }
}
?>
<div class="container whitebg">
  <form class="form-other" role="form" method="POST" action="">
    <h2><?php echo t('views.shifts.new_form.form_title') ?></h2>
    <br/>
    <input class="form-control form-control-top picker" type="text" required="" placeholder="<?php echo t('views.shifts.new_form.placeholder.date') ?>" id="date" name="date" <?php echo (isset($_POST['date'])) ? 'value="' . $_POST['date'] . '"' : "" ?>/>
    <input class="form-control form-control-top form-control-bottom" type="text" required="" placeholder="<?php echo t('views.shifts.new_form.placeholder.start_time') ?>" id="start_time" name="start_time" <?php echo (isset($_POST['start_time'])) ? 'value="' . $_POST['start_time'] . '"' : "" ?>/>
    <input class="form-control form-control-top form-control-bottom" type="text" required="" placeholder="<?php echo t('views.shifts.new_form.placeholder.end_time') ?>" id="end_time" name="end_time" <?php echo (isset($_POST['end_time'])) ? 'value="' . $_POST['end_time'] . '"' : "" ?>/>
    <input class="form-control form-control-bottom" type="text" placeholder="<?php echo t('views.shifts.new_form.placeholder.people_needed') ?>" id="people_needed" name="people_needed" <?php echo (isset($_POST['people_needed'])) ? 'value="' . $_POST['people_needed'] . '"' : "" ?>/>
    <br/>
    <input class="btn btn-lg btn-primary btn-block" type="submit" id="submit" name="submit" value="<?php echo t('views.shifts.new_form.add_button') ?>"/>
    <br/>
    <div class="alert alert-warning">
<?php
if ($end_before_start_error) {
  echo t('views.shifts.error_message.end_before_start_error');
} else {
  echo t('views.shifts.new_form.form_info');
}
?>
    </div>
  </form>
  <script type="text/javascript">
    $("#date").datepicker( { format: "yyyy-mm-dd", weekStart: 1 } );
  </script>
</div>
