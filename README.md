![](img/logo32.png)

# VMS
## Volunteer management system

With VMS you can:
* Manage your events
* Manage shifts for those events
* Create accounts for your volunteers/coworkers
* Let them apply for shifts they want to volunteer on
* Add system-wide notices to all users
* See at a glance how many people applied for each shift
* Sum up all shift hours per event per person
* And many more useful things...

# Notice

This is a very very old project of mine.

There is loads of bugs in it and I aim to improve it over time and modernize it(see Project roadmap section below).

## Project roadmap

In the future I would like to modernize this application.

First I will try and clean it up and separate things into neat classes.
Then add an autoloader and purge the project of many needless `require`s and `include`s.
After that move database connection to one place and use it where neccessary through dependency injection.
Also at some point I will introduce some sort of ORM.
From then on it will be neccessary to add a router to the application and make some sort of models and controllers.
A templating engine would be nice for views.
And at this point I want to write tests and try to make the application as robust as I can, both code reliability and security wise.

After all that is done, or somwhere in the meantime, a complete redesign of the application will be made in terms of UI and branding.

At one point I may add some kind of a frontend framework on it.

## Documentation

All code is undocumented for now. But that will be fixed in the future.
Some user guide for the application will be made either in application or as a LaTeX generated PDF.

## Setup

To setup this project on your server/machine you will have to do the following:

1. Install PHP7/PortgeSQL9.5, the newer the better. PostgreSQL module for PHP needs to be enabled. Sendmail setup might be needed so notification emails can be sent out.
2. Clone the project.
3. Create/Import the project database in PostgreSQL using your prefered method (`createdb vms; psql -d vms -f vms.sql` should do the trick). This will create a basic empty structure of the database and 1 admin user, Administrator (`username = administrator`). His password can be set by following step 6 below.
4. Edit `secrets.example.php` file to set your machine's info needed for the connection to the database, host url, emails and save it to `secrets.php`.
5. Run `php -S localhost:8000` in the project directory.
6. Fire up your preferred browser and go to `http://localhost:8000/index.php?content=users_lost_password&authorization_code=setup` to set the password for the user `administrator`.
7. Now you can log in and start playing with your new setup of **VMS**

## Issues, bugs and feature requests

All of these are welcome.

Happy hacking
