<?php

if(!ob_start("ob_gzhandler")) ob_start();

session_start();

if (!isset($_SESSION['user_id']) || $_SESSION['user_type'] > 1) {
  header("Location: index.php");
  die();
}

require 'autoload.php';
require 'database.php';
require 'translations.php';

$column_names = [
  'id' => t('views.users.table.number_sign'),
  'type' => t('views.users.table.user_type'),
  'email' => t('views.users.table.email'),
  'full_name' => t('views.users.table.full_name'),
  'telephone' => t('views.users.table.telephone'),
  'occupation' => t('views.users.table.occupation'),
  'personal_id_number' => t('views.users.table.personal_id_number'),
  'address' => t('views.users.table.address'),
  'authorized' => t('views.users.table.authorized')
];

/**
 * Helper function to map column name to translation text
 */
function map_column_names($input) {
    global $column_names;

    return isset($column_names[$input]) ? $column_names[$input] : $input;
}

$filename = "user_list_" . date('Y-m-d') . ".csv";
header("Content-Disposition: attachment; filename=\"$filename\"");
header("Content-Type: text/csv; charset=UTF-8");

$out = fopen("php://output", 'w');

$header_printed_flag = false;
$users = Users::all();
foreach ($users as $user) {
  $row = [
    $user['id'],
    $user['type'],
    $user['email'],
    $user['full_name'],
    $user['telephone'],
    $user['occupation'],
    $user['personal_id_number'],
    $user['address'],
    $user['authorized']
  ];
  if (!$header_printed_flag) {
    $first_line = array_map("map_column_names", array_keys($column_names));
    fputcsv($out, $first_line, ',', '"');
    $header_printed_flag = true;
  }
  fputcsv($out, array_values($row), ',', '"');
}
fclose($out);
exit(0);
