<?php

if (!isset($_SESSION['user_id']) || $_SESSION['user_type'] > 2) {
  header("Location: index.php");
  die();
}

$wrong_username = false;
$wrong_username_confirmation = false;
$wrong_password_confirmation = false;
$success = false;

if (isset($_POST["submit"])) {
  $new_user = [];
  $new_user['full_name'] = clean($_POST["full_name"]);
  $new_user['occupation'] = clean($_POST["occupation"]);
  $new_user['telephone'] = clean($_POST["telephone"]);
  $new_user['personal_id_number'] = clean($_POST["personal_id_number"]);
  $new_user['address'] = clean($_POST["address"]);
  $new_user['user_type'] = clean($_POST["user_type"]);
  $new_user['authorized'] = (isset($_POST["authorized"])) ? 't' : 'f';
  $alias = clean($_POST["alias"]);
  $username = clean($_POST["username"]);
  $username_confirmation = clean($_POST["username_confirmation"]);
  $password = clean($_POST["password"]);
  $password_confirmation = clean($_POST["password_confirmation"]);

  if ($username != $username_confirmation) {
    $wrong_username_confirmation = true;
  }
  if ($password != $password_confirmation) {
    $wrong_password_confirmation = true;
  } else {
    $new_user['password_hash'] = md5($password . $_secrets['password_salt']);
  }
  if (Users::find_by_email($username) || (!empty($alias) && Users::find_by_alias($alias))) {
    $wrong_username = true;
  } else {
    $new_user['email'] = $username;
    $new_user['alias'] = $alias;
  }
  if (!$wrong_username_confirmation && !$wrong_password_confirmation && !$wrong_username) {
    Users::create($new_user);
    LogEntries::create("[add_user] User '{$_SESSION['username']}' added a new user (email = '{$username}', full_name = '{$new_user['full_name']}').");
    $success = true;
  }
}
?>
<div class="container whitebg">
  <form class="form-other" role="form" method="POST" action="" onsubmit="return validateNewUserForm()">
    <h2><?php echo t('views.users.new_form.form_title') ?></h2>
    <br/>
    <span class=""><?php echo t('views.users.user_type') ?></span>
    <div class="radio">
      <label for="usertype3"><?php echo t('views.users.user_types.volunteer') ?></label>
      <input type="radio" name="user_type" id="usertype3" value="3" <?php echo (!isset($_POST['user_type']) || (isset($_POST['user_type']) && $_POST['user_type'] == 3)) ? "checked" : "" ?>>
    </div>
    <div class="radio">
      <label for="usertype2"><?php echo t('views.users.user_types.demonstrator') ?></label>
      <input type="radio" name="user_type" id="usertype2" value="2" <?php echo (isset($_POST['user_type']) && $_POST['user_type'] == 2) ? "checked" : "" ?>>
    </div>
    <div class="radio">
      <label for="usertype1"><?php echo t('views.users.user_types.administrator') ?></label>
      <input type="radio" name="user_type" id="usertype1" value="1" <?php echo (isset($_POST['user_type']) && $_POST['user_type'] == 1) ? "checked" : "" ?>>
    </div>
    <input class="form-control" type="username" placeholder="<?php echo t('views.users.new_form.placeholder.alias') ?>" id="alias" name="alias" <?php echo (isset($_POST['alias'])) ? 'value="' . $_POST['alias'] . '"' : "" ?>/>
    <br/>
    <input class="form-control form-control-top" type="username" placeholder="<?php echo t('views.users.new_form.placeholder.email') ?>" id="username" name="username" <?php echo (isset($_POST['username'])) ? 'value="' . $_POST['username'] . '"' : "" ?>/>
    <input class="form-control form-control-bottom" type="username" placeholder="<?php echo t('views.users.new_form.placeholder.email_confirmation') ?>" id="username_confirmation" name="username_confirmation" <?php echo (isset($_POST['username_confirmation'])) ? 'value="' . $_POST['username_confirmation'] . '"' : "" ?>/>
    <br/>
    <input class="form-control form-control-top" type="password" placeholder="<?php echo t('views.users.new_form.placeholder.password') ?>" id="password" name="password" <?php echo (isset($_POST['password'])) ? 'value="' . $_POST['password'] . '"' : "" ?>/>
    <input class="form-control form-control-bottom" type="password" placeholder="<?php echo t('views.users.new_form.placeholder.password_confirmation') ?>" id="password_confirmation" name="password_confirmation" <?php echo (isset($_POST['password_confirmation'])) ? 'value="' . $_POST['password_confirmation'] . '"' : "" ?>/>
    <br/>
    <input class="form-control form-control-top" type="text" placeholder="<?php echo t('views.users.new_form.placeholder.full_name') ?>" id="full_name" name="full_name" <?php echo (isset($_POST['full_name'])) ? 'value="' . $_POST['full_name'] . '"' : "" ?>/>
    <input class="form-control form-control-top form-control-bottom" type="text" placeholder="<?php echo t('views.users.new_form.placeholder.occupation') ?>" id="occupation" name="occupation" <?php echo (isset($_POST['occupation'])) ? 'value="' . $_POST['occupation'] . '"' : "" ?>/>
    <input class="form-control form-control-bottom" type="text" placeholder="<?php echo t('views.users.new_form.placeholder.telephone') ?>" id="telephone" name="telephone" <?php echo (isset($_POST['telephone'])) ? 'value="' . $_POST['telephone'] . '"' : "" ?>/>
    <div class="checkbox">
      <label for="authorized"><?php echo t('views.users.new_form.authorized_checkbox_label') ?></label>
      <input id="authorized" name="authorized" type="checkbox"  <?php echo (isset($_POST['authorized']) && $_POST['authorized'] == 1) ? "checked" : "" ?> value="1" >
    </div>
    <input class="btn btn-lg btn-primary btn-block" type="submit" id="submit" name="submit" value="<?php echo t('views.users.new_form.add_button') ?>"/>
    <br/>
    <div id="warning_message" class="alert alert-warning">
<?php
if ($wrong_username) {
  echo t('views.users.error_message.wrong_username');
} elseif ($wrong_username_confirmation) {
  echo t('views.users.error_message.wrong_username_confirmation');
} elseif ($wrong_password_confirmation) {
  echo t('views.users.error_message.wrong_password_confirmation');
} elseif ($success) {
  echo t('views.users.new_form.created_success');
} else {
  echo t('views.users.new_form.new_info');
}
?>
    </div>
  </form>
</div>
