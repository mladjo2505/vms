<?php

if (isset($_GET["shift_id"])) {
  $id = $_GET["shift_id"];
}

if ($id && (!isset($_SESSION['user_id']) || !(isset($_SESSION['user_type']) && $_SESSION['user_type'] == 1))) {
  header("Location: index.php");
  die();
}

$end_before_start_error = false;
$shift = Shifts::find_by_id($id);
if (isset($_POST["submit"])) {
  $updated_shift = [];
  $updated_shift['date'] = clean($_POST["date"]);
  $updated_shift['start_time'] = clean($_POST["start_time"]);
  $updated_shift['end_time'] = clean($_POST["end_time"]);
  $updated_shift['people_needed'] = clean($_POST["people_needed"]);
  $updated_shift['event_id'] = $shift['event_id'];
  $shift_start = strtotime("{$updated_shift['date']} {$updated_shift['start_time']}");
  $shift_end = strtotime("{$updated_shift['date']} {$updated_shift['end_time']}");
  $shift_duration = $shift_end - $shift_start;
  if ($shift_duration < 0) {
    $end_before_start_error = true;
  } elseif (Shifts::update($id, $updated_shift)) {
    LogEntries::create("[update_shift] User '{$_SESSION['username']}' changed shift (id = {$id}).");
    header("Location: index.php?content=shifts_index&event_id={$shift['event_id']}");
    die();
  }
}
?>
<div class="container whitebg">
  <form class="form-other" role="form" method="POST" action="">
    <h2><?php echo t('views.shifts.edit_form.form_title') ?></h2>
    <br/>
    <input class="form-control form-control-top picker" type="text" required="" value="<?php echo (isset($_POST['date'])) ? $_POST['date'] : $shift['date'] ?>" id="date" name="date"/>
    <input class="form-control form-control-top form-control-bottom" type="text" required="" value="<?php echo (isset($_POST['start_time'])) ? $_POST['start_time'] : strftime('%H:%M', strtotime($shift['start_time'])) ?>" id="start_time" name="start_time"/>
    <input class="form-control form-control-top form-control-bottom" type="text" required="" value="<?php echo (isset($_POST['end_time'])) ? $_POST['end_time'] : strftime('%H:%M', strtotime($shift['end_time'])) ?>" id="end_time" name="end_time"/>
    <input class="form-control form-control-bottom" type="text" value="<?php echo (isset($_POST['people_needed'])) ? $_POST['people_needed'] : $shift['people_needed'] ?>" id="people_needed" name="people_needed"/>
    <br/>
    <input class="btn btn-lg btn-primary btn-block" type="submit" id="submit" name="submit" value="<?php echo t('views.shifts.edit_form.save_changes_button') ?>"/>
    <br/>
    <div class="alert alert-warning">
<?php
if ($end_before_start_error) {
  echo t('views.shifts.error_message.end_before_start_error');
} else {
  echo t('views.shifts.edit_form.form_info');
}
?>
    </div>
  </form>
  <script type="text/javascript">
    $("#date").datepicker( { format: "yyyy-mm-dd", weekStart: 1 } );
  </script>
</div>
