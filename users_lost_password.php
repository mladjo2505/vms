<?php

$wrong_password_confirmation = false;

if (!isset($_GET["authorization_code"])) {
  header("Location: index.php");
  die();
} else {
  $authorization_code = clean($_GET["authorization_code"]);
}
$user = Users::find_by('authorization_code', $authorization_code);
if (!$user) {
  header("Location: index.php");
  die();
} else {
  if (time() > strtotime('+1 hour', strtotime($user['password_reset_at']))) {
    Users::clear_password_reset($user);
    header("Location: index.php");
    die();
  }
}

if (isset($_POST["submit"])) {
  $new_password = clean($_POST["password"]);
  $new_password_confirmation = clean($_POST["password_confirmation"]);
  $user_id = $_POST["user_id"];

  $wrong_password_confirmation = $new_password != $new_password_confirmation;
  if (!$wrong_password_confirmation) {
    $password_hash = md5($new_password . $_secrets['password_salt']);
    $updated_user = ['password_hash' => $password_hash];
    if (Users::update($user_id, $updated_user)) {
      Users::clear_password_reset($user);
      LogEntries::create("[password_recovery] User '{$user['full_name']}' (email= '{$user['email']}') changed his password after reset request.");
      header("Location: index.php");
      die();
    }
  }
}
?>
<form id="lost_password_form" class="form-signin" role="form" method="POST" action="" onsubmit="return validateLostPassForm()">
  <input class="hidden" type="text" value="<?php echo $user['id']; ?>" id="user_id" name="user_id"/>
  <input class="form-control" type="password" placeholder="<?php echo t('views.login.placeholder.password') ?>" id="password" name="password"/>
  <input class="form-control" type="password" placeholder="<?php echo t('views.login.placeholder.password_confirmation') ?>" id="password_confirmation" name="password_confirmation"/>
  <input class="btn btn-lg btn-primary btn-block" type="submit" id="submit" name="submit" value="<?php echo t('views.login.set_new_password') ?>"/>
  <br/>
  <div id="warning_message" class="alert alert-warning">
<?php
if ($wrong_password_confirmation) {
  echo t('views.login.wrong_password_confirmation');
} else {
  echo t('views.login.set_new_password');
}
?>
  </div>
</form>
