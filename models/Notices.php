<?php

class Notices {
  public function last($n = 5) {
    $notices = [];
    $select_notices_query = "
      SELECT id, title, contents
      FROM notices
      ORDER BY created_at DESC
      LIMIT {$n}
    ";
    if (!$select_notices_result = pg_query($select_notices_query)) {
      die("Error executing query. " . pg_last_error());
    }
    if (pg_num_rows($select_notices_result) != 0) {
      while ($notice = pg_fetch_assoc($select_notices_result)) {
        array_push($notices, $notice);
      }
    }
    return $notices;
  }

  public function find_by_id($id) {
    $select_notice_query = "
      SELECT id, title, contents
      FROM notices
      WHERE id = {$id}
    ";
    $select_notice_result = pg_query($select_notice_query);
    if (!$select_notice_result) {
      die("Error executing query. " . pg_last_error());
    }
    $notice = pg_fetch_assoc($select_notice_result);
    return $notice;
  }

  public function create($title, $contents, $user_id) {
    $timestamp = date('Y-m-d H:i:s', time());
    $insert_notice_query = "
      INSERT INTO notices(user_id, title, contents, created_at)
      VALUES ({$user_id}, '{$title}', '{$contents}', '{$timestamp}')
      RETURNING id
    ";
    $insert_notice_result = pg_query($insert_notice_query);
    if (!$insert_notice_result) {
      die("Error executing query. " . pg_last_error());
    }
    return $insert_notice_result;
  }

  public function delete($id) {
    $delete_notice_query = "
      DELETE FROM notices
      WHERE id = '{$id}'
    ";
    $delete_notice_result = pg_query($delete_notice_query);
    if (!$delete_notice_result) {
      die("Error executing query. " . pg_last_error());
    }
    return $delete_notice_result;
  }
}
