<?php

class Users {
  public function all() {
    $users = [];
    $select_users_query = "
      SELECT *
      FROM users
      ORDER BY type, full_name
    ";
    if (!$select_users_result = pg_query($select_users_query)) {
      die("Error executing query." . pg_last_error());
    }
    while ($user = pg_fetch_assoc($select_users_result)) {
      array_push($users, $user);
    }
    return $users;
  }

  public function find_by_id($id) {
    $select_user_query = "
      SELECT *
      FROM users
      WHERE id = '{$id}'
    ";
    $select_user_result = pg_query($select_user_query);
    if (!$select_user_result) {
      die("Error executing query." . pg_last_error());
    }
    $user = pg_fetch_assoc($select_user_result);
    return $user;
  }

  public function find_by_username($username) {
    $select_user_query = "
      SELECT *
      FROM users
      WHERE email = '{$username}' OR alias = '{$username}'
    ";
    $select_user_result = pg_query($select_user_query);
    if (!$select_user_result) {
      die("Error executing query." . pg_last_error());
    }
    if (pg_num_rows($select_user_result) != 0) {
      $user = pg_fetch_assoc($select_user_result);
      return $user;
    } else {
      return false;
    }
  }

  public function find_by_full_name($full_name) {
    $users = [];
    $search_name = strtoupper($full_name);
    $select_users_query = "
      SELECT *
      FROM users
      WHERE UPPER(full_name) LIKE '%{$search_name}%'
    ";
    if (!$select_users_result = pg_query($select_users_query)) {
      die("Error executing query." . pg_last_error());
    }
    while ($user = pg_fetch_assoc($select_users_result)) {
      array_push($users, $user);
    }
    return $users;
  }

  public function find_by($field, $value) {
    $select_user_query = "
      SELECT *
      FROM users
      WHERE {$field} = '{$value}'
    ";
    $select_user_result = pg_query($select_user_query);
    if (!$select_user_result) {
      die("Error executing query." . pg_last_error());
    }
    if (pg_num_rows($select_user_result) != 0) {
      $user = pg_fetch_assoc($select_user_result);
      return $user;
    } else {
      return false;
    }
  }

  public function find_by_email($email) {
    return Users::find_by('email', $email);
  }

  public function find_by_alias($alias) {
    return Users::find_by('alias', $alias);
  }

  public function update($id, $user) {
    $fields = [
      'type',
      'email',
      'password_hash',
      'full_name',
      'telephone',
      'occupation',
      'personal_id_number',
      'address',
      'authorized',
      'authorization_code',
      'password_reset_at',
      'alias'
    ];
    $changes = [];
    foreach ($fields as $field) {
      if (isset($user[$field])) {
        if ($user[$field] == "NULL") {
          array_push($changes, "{$field} = NULL");
        } else {
          array_push($changes, "{$field} = '$user[$field]'");
        }
      }
    }
    if (empty($changes)) {
      return true;
    }
    $update_user_query = "
      UPDATE users
      SET " .
        implode(", ", $changes) . "
      WHERE id = '{$id}'
    ";
    $update_user_result = pg_query($update_user_query);
    if (!$update_user_result) {
        die("Error executing query." . pg_last_error());
    }
    return $update_user_result;
  }

  public function create($user) {
    $timestamp = date('Y-m-d H:i:s', time());
    $insert_user_query = "
      INSERT INTO users (type, email, password_hash, full_name, telephone, occupation, authorized, authorization_code, alias, created_at)
      VALUES ('{$user['user_type']}', '{$user['email']}', '{$user['password_hash']}', '{$user['full_name']}', '{$user['telephone']}', '{$user['occupation']}', '{$user['authorized']}', '', '{$user['alias']}', '{$timestamp}')
    ";
    $insert_user_result = pg_query($insert_user_query);
    if (!$insert_user_result) {
        die("Error executing query." . pg_last_error());
    }
    return $insert_user_result;
  }

  public function clear_password_reset($user) {
    Users::password_reset($user, true);
  }

  public function password_reset($user, $clear = false) {
    global $_secrets;

    $user_id = $user['id'];
    $updated_user = [
      'password_reset_at' => "NULL",
      'authorization_code' => ""
    ];
    if (!$clear) {
      $updated_user['password_reset_at'] = strftime('%F %T', time());
      $updated_user['authorization_code'] = md5($updated_user['password_reset_at'] . $_secrets['password_salt']);
    }
    $update_success = Users::update($user_id, $updated_user);
    if ($update_success) {
      Users::send_password_reset_email($user['email'], $updated_user['authorization_code']);
    }
    return $update_success;
  }

  public function send_password_reset_email($send_to, $authorization_code) {
    global $_secrets;

    $subject = t('views.login.password_reset.email.subject');
    $from = $_secrets['from_email'];
    $reply_to = $_secrets['reply_to_email'];
    $headers = "From: " . "<" . $from .">\r\n";
    $headers .= "Reply-To: " . $reply_to . "\r\n";
    $headers .= "Return-path: " . $reply_to;
    $message = t('views.login.password_reset.email.message') . "\r\n\r\n";
    $message .= $_secrets['host_address'] . "index.php?content=users_lost_password&authorization_code=" . $authorization_code;
    mail($send_to, $subject, $message, $headers, "-f {$from}");
  }
}
