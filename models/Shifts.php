<?php

class Shifts {
  public function all_for_event($event_id) {
    $shifts = [];
    $select_shifts_query = "
      SELECT *
      FROM shifts
      WHERE event_id = {$event_id}
      ORDER BY date, start_time
    ";
    if (!$select_shifts_result = pg_query($select_shifts_query)) {
      die("Error executing query." . pg_last_error());
    }
    while ($shift = pg_fetch_assoc($select_shifts_result)) {
      array_push($shifts, $shift);
    }
    return $shifts;
  }

  public function get_criticalness_for_date($target_date) {
    $date = date('Y-m-d', $target_date);
    $lowest_applied_percentage = 100;
    $select_shifts_status_query = "
      SELECT shifts.id, shifts.people_needed, count(*) AS people_applied
      FROM
        shifts
        JOIN
        user_shifts ON shifts.id = user_shifts.shift_id
      WHERE shifts.date = '{$date}'
      GROUP BY shifts.id
    ";
    if (!$select_shifts_status_result = pg_query($select_shifts_status_query)) {
      die("Error executing query." . pg_last_error());
    }
    if (pg_num_rows($select_shifts_status_result) == 0) {
      return 0;
    }
    while ($shift_status = pg_fetch_assoc($select_shifts_status_result)) {
      $shift_applied_percentage = (int)($shift_status['people_applied']) / (int)($shift_status['people_needed']) * 100;
      $lowest_applied_percentage = min($lowest_applied_percentage, $shift_applied_percentage);
    }
    return $lowest_applied_percentage;
  }

  public function get_shifits_info_for_date_and_user($target_date, $user_id) {
    $date = date('Y-m-d', $target_date);
    $shifts_info = [];
    $select_shifts_info_query = "
      SELECT
        shifts.event_id,
        events.name AS event_name,
        shifts.id,
        shifts.start_time,
        shifts.end_time,
        shifts.people_needed,
        count(user_shifts.id) AS people_applied
      FROM
        shifts
        JOIN
        events ON shifts.event_id = events.id
        LEFT OUTER JOIN
        user_shifts ON user_shifts.shift_id = shifts.id
      WHERE
        shifts.date = '{$date}'
      GROUP BY events.name, shifts.id
      ORDER BY shifts.event_id, shifts.start_time
    ";
    if (!$select_shifts_info_result = pg_query($select_shifts_info_query)) {
      die("Error executing query." . pg_last_error());
    }
    $select_user_applied_shifts_query = "
      SELECT shifts.id
      FROM
        user_shifts
        JOIN
        shifts ON user_shifts.shift_id = shifts.id
      WHERE
        shifts.date = '{$date}' AND
        user_shifts.user_id = {$user_id}
    ";
    if (!$select_user_applied_shifts_result = pg_query($select_user_applied_shifts_query)) {
      die("Error executing query." . pg_last_error());
    }
    $user_applied_shifts = [];
    while ($shift = pg_fetch_assoc($select_user_applied_shifts_result)) {
      array_push($user_applied_shifts, $shift['id']);
    }
    while ($shift_info = pg_fetch_assoc($select_shifts_info_result)) {
      $shift_info['user_applied'] = in_array($shift_info['id'], $user_applied_shifts);
      array_push($shifts_info, $shift_info);
    }
    return $shifts_info;
  }

  public function all_for_date($date) {
    $shifts = [];
    $select_shifts_query = "
      SELECT *
      FROM shifts
      WHERE date = '{$date}'
      ORDER BY event_id, start_time
    ";
    if (!$select_shifts_result = pg_query($select_shifts_query)) {
      die("Error executing query." . pg_last_error());
    }
    while ($shift = pg_fetch_assoc($select_shifts_result)) {
      array_push($shifts, $shift);
    }
    return $shifts;
  }

  public function create_multiple($event_id, $shifts) {
    foreach ($shifts as &$shift) {
      $shift['event_id'] = $event_id;
    }
    function shift_to_value_string($shift) {
      return "('{$shift['event_id']}', '{$shift['date']}', '{$shift['start_time']}', '{$shift['end_time']}', '{$shift['people_needed']}')";
    }
    $insert_shifts_query = "
      INSERT INTO shifts(event_id, date, start_time, end_time, people_needed)
      VALUES " . implode(", ", array_map("shift_to_value_string", $shifts)) . "
    ";
    $insert_shifts_result = pg_query($insert_shifts_query);
    if (!$insert_shifts_result) {
      die("Error executing query." . pg_last_error());
    }
    return $insert_shifts_result;
  }

  public function compile_shifts($shift_hours, $people_needed, $start_date, $end_date) {
    $shifts_info = [];
    $shift_hours_by_shift = explode(',', $shift_hours);
    $people_needed_by_shift = explode(',', $people_needed);
    if (count($shift_hours_by_shift) != count($people_needed_by_shift)) {
      $shifts_info['count_mismatch_error'] = true;
      return $shifts_info;
    }
    for ($i = 0; $i < count($shift_hours_by_shift); $i++) {
      $shift_info = [];
      list($shift_info['start_time'], $shift_info['end_time']) = explode('-', $shift_hours_by_shift[$i]);
      $shift_info['people_needed'] = $people_needed_by_shift[$i];
      array_push($shifts_info, $shift_info);
    }
    $start_date = strtotime($start_date);
    $end_date = strtotime($end_date);
    $insert_shifts = "
      INSERT INTO shifts(event_id, date, start_time, end_time, people_needed)
      VALUES ";
    $shifts = [];
    for ($current_day = $start_date; $current_day <= $end_date; $current_day = strtotime('+1 day', $current_day)) {
      $date = date('Y-m-d', $current_day);
      foreach ($shifts_info as $shift_info) {
        $shift = [];
        $shift['start_time'] = $shift_info['start_time'];
        $shift['end_time'] = $shift_info['end_time'];
        $shift['people_needed'] = $shift_info['people_needed'];
        $shift['date'] = $date;
        array_push($shifts, $shift);
      }
    }
    return $shifts;
  }

  public function delete_all_for_event($event_id) {
    // TODO: Maybe ensure dependent destroy by DB design
    // by adding cascade destroy for foreign keys
    if (!UserShifts::delete_all_for_event($event_id)) {
      return false;
    }
    $delete_shifts_query = "
      DELETE FROM shifts
      WHERE event_id = {$event_id}
    ";
    $delete_shifts_result = pg_query($delete_shifts_query);
    if (!$delete_shifts_result) {
      die("Error executing query. " . pg_last_error());
    }
    return $delete_shifts_result;
  }

  public function delete($id) {
    // TODO: Maybe ensure dependent destroy by DB design
    // by adding cascade destroy for foreign keys
    if (!UserShifts::delete_all_for_shift($id)) {
      return false;
    }
    $delete_shift_query = "
      DELETE FROM shifts
      WHERE id = {$id}
    ";
    $delete_shift_result = pg_query($delete_shift_query);
    if (!$delete_shift_result) {
      die("Error executing query. " . pg_last_error());
    }
    return $delete_shift_result;
  }

  public function find_by_id($id) {
    $select_shift_query = "
      SELECT *
      FROM shifts
      WHERE id = '{$id}'
    ";
    $select_shift_result = pg_query($select_shift_query);
    if (!$select_shift_result) {
      die("Error executing query." . pg_last_error());
    }
    $shift = pg_fetch_assoc($select_shift_result);
    return $shift;
  }

  public function create($shift) {
    $insert_shift_query = "
      INSERT INTO shifts (event_id, date, start_time, end_time, people_needed)
      VALUES ('{$shift['event_id']}', '{$shift['date']}', '{$shift['start_time']}', '{$shift['end_time']}', '{$shift['people_needed']}')
    ";
    $insert_shift_result = pg_query($insert_shift_query);
    if (!$insert_shift_result) {
        die("Error executing query." . pg_last_error());
    }
    return $insert_shift_result;
  }

  public function update($id, $shift) {
    $fields = [
      'date',
      'start_time',
      'end_time',
      'people_needed'
    ];
    $changes = [];
    foreach ($fields as $field) {
      if (isset($shift[$field])) {
        if ($shift[$field] == "NULL") {
          array_push($changes, "{$field} = NULL");
        } else {
          array_push($changes, "{$field} = '$shift[$field]'");
        }
      }
    }
    if (empty($changes)) {
      return true;
    }
    $update_shift_query = "
      UPDATE shifts
      SET " .
        implode(", ", $changes) . "
      WHERE id = '{$id}'
    ";
    $update_shift_result = pg_query($update_shift_query);
    if (!$update_shift_result) {
        die("Error executing query." . pg_last_error());
    }
    return $update_shift_result;
  }
}
