<?php

class Events {
  public function all() {
    $events = [];
    $select_events_query = "
      SELECT *
      FROM events
    ";
    if (!$select_events_result = pg_query($select_events_query)) {
      die("Error executing query." . pg_last_error());
    }
    while ($event = pg_fetch_assoc($select_events_result)) {
      array_push($events, $event);
    }
    return $events;
  }

  public function find_by_id($id) {
    $select_event_query = "
      SELECT *
      FROM events
      WHERE id = {$id}
    ";
    $select_event_result = pg_query($select_event_query);
    if (!$select_event_result) {
      die("Error executing query. " . pg_last_error());
    }
    $event = pg_fetch_assoc($select_event_result);
    return $event;
  }

  // TODO: Do this as a transaction in case of linked shift creation
  public function create($event, $create_shifts = false, $shifts = []) {
    $insert_event_query = "
      INSERT INTO events(name, description, start_date, end_date)
      VALUES ('{$event['name']}', '{$event['description']}', '{$event['start_date']}', '{$event['end_date']}')
    ";
    $insert_event_result = pg_query($insert_event_query);
    if (!$insert_event_result) {
        die("Error executing query." . pg_last_error());
    }
    $event_id = Events::last_inserted_id();
    if ($create_shifts) {
      return Shifts::create_multiple($event_id, $shifts);
    }
    return $insert_event_result;
  }

  public function last_inserted_id() {
    $select_id_query = "
      SELECT LASTVAL() AS id
    ";
    $select_id_result = pg_query($select_id_query);
    if (!$select_id_result) {
      die("Error executing query." . pg_last_error());
      return false;
    }
    $id = pg_fetch_assoc($select_id_result)['id'];
    return $id;
  }

  public function create_with_shifts($event, $shifts) {
    return Events::create($event, true, $shifts);
  }

  public function delete($id) {
    // TODO: Maybe ensure dependent destroy by DB design
    // by adding cascade destroy for foreign keys
    if (!Shifts::delete_all_for_event($id)) {
      return false;
    }
    $delete_event_query = "
      DELETE FROM events
      WHERE id = '{$id}'
    ";
    $delete_event_result = pg_query($delete_event_query);
    if (!$delete_event_result) {
      die("Error executing query. " . pg_last_error());
    }
    return $delete_event_result;
  }

  public function update($id, $event) {
    $fields = [
      'name',
      'description',
      'start_date',
      'end_date'
    ];
    $changes = [];
    foreach ($fields as $field) {
      if (isset($event[$field])) {
        if ($event[$field] == "NULL") {
          array_push($changes, "{$field} = NULL");
        } else {
          array_push($changes, "{$field} = '$event[$field]'");
        }
      }
    }
    if (empty($changes)) {
      return true;
    }
    $update_event_query = "
      UPDATE events
      SET " .
        implode(", ", $changes) . "
      WHERE id = '{$id}'
    ";
    $update_event_result = pg_query($update_event_query);
    if (!$update_event_result) {
        die("Error executing query." . pg_last_error());
    }
    return $update_event_result;
  }
}
