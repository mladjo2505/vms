<?php

class LogEntries {
  public function last($n = 150) {
    $log_entries = [];
    $select_log_entries_query = "
      SELECT *
      FROM logs
      ORDER BY created_at DESC
      LIMIT {$n}
    ";
    if (!$log_entries_result = pg_query($select_log_entries_query)) {
      die("Error executing query. " . pg_last_error());
    }
    if (pg_num_rows($log_entries_result) != 0) {
      while ($log_entry = pg_fetch_assoc($log_entries_result)) {
        array_push($log_entries, $log_entry);
      }
    }
    return $log_entries;
  }

  public function create($message) {
    $message = clean($message);
    $timestamp = date('Y-m-d H:i:s', time());
    $insert_log_entry_query = "
      INSERT INTO logs(message, created_at)
      VALUES ('{$message}', '{$timestamp}')
    ";
    $insert_log_entry_result = pg_query($insert_log_entry_query);
    if (!$insert_log_entry_result) {
      die("Error executing query. " . pg_last_error());
    }
    return $insert_log_entry_result;
  }
}
