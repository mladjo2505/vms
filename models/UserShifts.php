<?php

class UserShifts {
  public function all_for_shift($shift_id) {
    $user_shifts = [];
    $select_user_shifts_query = "
      SELECT *
      FROM user_shifts
      WHERE shift_id = {$shift_id}
    ";
    if (!$select_user_shifts_result = pg_query($select_user_shifts_query)) {
      die("Error executing query." . pg_last_error());
    }
    while ($user_shift = pg_fetch_assoc($select_user_shifts_result)) {
      array_push($user_shifts, $user_shift);
    }
    return $user_shifts;
  }

  public function all_for_user_in_date_range($user_id, $start_date, $end_date) {
    $user_shifts = [];
    $select_user_shifts_query = "
      SELECT user_shifts.*
      FROM
        user_shifts
        JOIN
        shifts ON user_shifts.shift_id = shifts.id
      WHERE
        user_shifts.user_id = {$user_id} AND
        shifts.date < '{$end_date}' AND
        shifts.date >= '{$start_date}'
    ";
    if (!$select_user_shifts_result = pg_query($select_user_shifts_query)) {
      die("Error executing query." . pg_last_error());
    }
    while ($user_shift = pg_fetch_assoc($select_user_shifts_result)) {
      array_push($user_shifts, $user_shift);
    }
    return $user_shifts;
  }

  public function all_for_date($user_id, $date) {
    $user_shifts = [];
    $select_user_shifts_query = "
      SELECT user_shifts.shift_id, user_shifts.id
      FROM
        user_shifts
        JOIN
        shifts ON user_shifts.shift_id = shifts.id
      WHERE
        user_shifts.user_id = {$user_id} AND
        shifts.date = '{$date}'
    ";
    if (!$select_user_shifts_result = pg_query($select_user_shifts_query)) {
      die("Error executing query." . pg_last_error());
    }
    while ($user_shift = pg_fetch_assoc($select_user_shifts_result)) {
      array_push($user_shifts, $user_shift);
    }
    return $user_shifts;
  }

  public function delete_all_for_event($event_id) {
    $delete_user_shifts_query = "
      DELETE FROM user_shifts
      WHERE shift_id IN (
        SELECT id
        FROM shifts
        WHERE event_id = {$event_id}
      )
    ";
    $delete_user_shifts_result = pg_query($delete_user_shifts_query);
    if (!$delete_user_shifts_result) {
      die("Error executing query. " . pg_last_error());
    }
    return $delete_user_shifts_result;
  }

  public function delete_all_for_shift($shift_id) {
    $delete_user_shifts_query = "
      DELETE FROM user_shifts
      WHERE shift_id = {$shift_id}
    ";
    $delete_user_shifts_result = pg_query($delete_user_shifts_query);
    if (!$delete_user_shifts_result) {
      die("Error executing query. " . pg_last_error());
    }
    return $delete_user_shifts_result;
  }

  public function delete($id) {
    $delete_user_shift_query = "
      DELETE FROM user_shifts
      WHERE id = {$id}
    ";
    $delete_user_shift_result = pg_query($delete_user_shift_query);
    if (!$delete_user_shift_result) {
      die("Error executing query. " . pg_last_error());
    }
    return $delete_user_shift_result;
  }

  public function find_by_id($id) {
    $select_user_shift_query = "
      SELECT *
      FROM user_shifts
      WHERE id = '{$id}'
    ";
    $select_user_shift_result = pg_query($select_user_shift_query);
    if (!$select_user_shift_result) {
      die("Error executing query." . pg_last_error());
    }
    $user_shift = pg_fetch_assoc($select_user_shift_result);
    return $user_shift;
  }

  public function create($user_shift) {
    $insert_user_shift_query = "
      INSERT INTO user_shifts(shift_id, user_id)
      VALUES ({$user_shift['shift_id']}, {$user_shift['user_id']})
    ";
    $insert_user_shift_result = pg_query($insert_user_shift_query);
    if (!$insert_user_shift_result) {
        die("Error executing query." . pg_last_error());
    }
    return $insert_user_shift_result;
  }

  public function create_multiple($user_shifts) {
    function user_shift_to_value_string($user_shift) {
      return "({$user_shift['shift_id']}, {$user_shift['user_id']})";
    }
    $insert_user_shifts_query = "
      INSERT INTO user_shifts(shift_id, user_id)
      VALUES " . implode(", ", array_map("user_shift_to_value_string", $user_shifts)) . "
    ";
    $insert_user_shifts_result = pg_query($insert_user_shifts_query);
    if (!$insert_user_shifts_result) {
      die("Error executing query." . pg_last_error());
    }
    return $insert_user_shifts_result;
  }

  public function delete_multiple_by_user_and_shift_ids($user_id, $ids) {
    $delete_user_shifts_query = "
      DELETE FROM user_shifts
      WHERE
        user_id = {$user_id} AND
        shift_id IN (" . implode(", ", $ids) . ")
    ";
    $delete_user_shifts_result = pg_query($delete_user_shifts_query);
    if (!$delete_user_shifts_result) {
      die("Error executing query. " . pg_last_error());
    }
    return $delete_user_shifts_result;
  }
}
