<?php

class Reports {
  public function user_shifts_sum($event_id, $user_type) {
    $user_shifts = [];
    $user_type_query_part = $user_type == 0
      ? ""
      : "AND users.type = {$user_type}";
    $select_user_shifts_query = "
      SELECT
        users.*,
        (shifts.end_time - shifts.start_time) AS shift_hours,
        1 AS shift_count
      FROM
        users
        JOIN
        user_shifts ON user_shifts.user_id = users.id
        JOIN
        shifts ON shifts.id = user_shifts.shift_id
      WHERE
        shifts.event_id = {$event_id}
        {$user_type_query_part}
    ";
    if (!$select_user_shifts_result = pg_query($select_user_shifts_query)) {
      die("Error executing query." . pg_last_error());
    }
    while ($user_shift = pg_fetch_assoc($select_user_shifts_result)) {
      $shift_hours = strtotime($user_shift["shift_hours"]);
      $shift_hours_float = (float)strftime('%H', $shift_hours) + (float)strftime('%M', $shift_hours)/60.0;
      if (isset($user_shifts[$user_shift['id']])) {
        $user_shifts[$user_shift['id']]['hours'] += $shift_hours_float;
        $user_shifts[$user_shift['id']]['shifts'] += $user_shift['shift_count'];
      } else {
        $user_shifts[$user_shift['id']] = $user_shift;
        $user_shifts[$user_shift['id']]['hours'] = $shift_hours_float;
        $user_shifts[$user_shift['id']]['shifts'] = $user_shift['shift_count'];
      }
    }
    return $user_shifts;
  }
}
