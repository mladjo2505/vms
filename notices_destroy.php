<?php

if (!isset($_SESSION['user_id']) || !(isset($_SESSION['user_type']) && $_SESSION['user_type'] < 3)) {
  header("Location: index.php");
  die();
}

$id = clean($_GET['id']);
if(Notices::delete($id)) {
  LogEntries::create("[remove_notice] User '{$_SESSION['username']}' removed a notice (id = {$id}).");
  header("Location: index.php?content=notices_index");
  die();
}
