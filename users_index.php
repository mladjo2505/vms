<?php

function user_type_to_translation_key($type) {
  switch ($type) {
    case 3:
      return 'volunteer';
    case 2:
      return 'demonstrator';
    case 1:
      return 'administrator';
    default:
     return 'unknown';
  }
}

if (!isset($_SESSION['user_id']) || $_SESSION['user_type'] > 1) {
  header("Location: index.php");
  die();
}

$users = Users::all();
?>
<div class="whitebg-full">
  <a class="pull-left btn btn-md btn-primary" href="users_new.php" target="_blank"><span class='glyphicon glyphicon-plus'></span> <?php echo t('views.users.table.add_user_button') ?></a>
  <a class="pull-right btn btn-md btn-primary" href="users_export.php" target="_blank"><span class='glyphicon glyphicon-download'></span> <?php echo t('views.users.table.download_button') ?></a>
  <br/>
  <br/>
  <table class="table table-striped table-bordered table-condensed">
    <thead>
      <tr>
        <th><b><?php echo t('views.users.table.number_sign') ?></b></th>
        <th><b><?php echo t('views.users.table.user_type') ?></b></th>
        <th><b><?php echo t('views.users.table.full_name') ?></b></th>
        <th><b><?php echo t('views.users.table.telephone') ?></b></th>
        <th><b><?php echo t('views.users.table.email') ?></b></th>
        <th><b><?php echo t('views.users.table.occupation') ?></b></th>
        <th><b><?php echo t('views.users.table.personal_id_number') ?></b></th>
        <th><b><?php echo t('views.users.table.address') ?></b></th>
        <th><b><?php echo t('views.users.table.alias') ?></b></th>
        <th><b><?php echo t('views.users.table.authorized') ?></b></th>
        <th><b><?php echo t('views.users.table.date_of_registration') ?></b></th>
      </tr>
    </thead>
    <tbody>
<?php
foreach ($users as $user) {
    $user_type = t('views.users.user_types.' . user_type_to_translation_key($user['type']));
    $authorized = ($user['authorized'] == 't') ? t('yes') : t('no');
    $date = date('Y-m-d', strtotime($user['created_at']));
?>
      <tr>
        <td><?php echo $user['id']; ?></td>
        <td><?php echo $user_type; ?></td>
        <td><?php echo $user['full_name']; ?></td>
        <td><?php echo $user['telephone']; ?></td>
        <td><?php echo $user['email']; ?></td>
        <td><?php echo $user['occupation']; ?></td>
        <td><?php echo $user['personal_id_number']; ?></td>
        <td><?php echo $user['address']; ?></td>
        <td><?php echo $user['alias']; ?></td>
        <td><?php echo $authorized; ?></td>
        <td><?php echo $date; ?></td>
      </tr>
<?php
}
?>
    </tbody>
  </table>
</div>
