<nav class="navbar navbar-default" role="navigation">
  <div class="container-fluid">
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li <?php if ($content == "" ) echo 'class="active"'; ?>>
          <a href="index.php"><span class='glyphicon glyphicon-home'></span></a>
        </li>
        <li <?php if ($content == "notices_index" ) echo 'class="active"'; ?>>
          <a href="index.php?content=notices_index"><span class='glyphicon glyphicon-info-sign'> <?php echo t('views.notices.title') ?></span></a>
        </li>
<?php
if ($_SESSION['user_type'] < 3) {
  if ($_SESSION['user_type'] == 1) {
?>
        <li <?php if ($content == "users_index" || $content == "users_new") echo 'class="active"'; ?>>
          <a href="index.php?content=users_index"><span class='glyphicon glyphicon-list'> <?php echo t('views.users.title') ?></span></a>
        </li>
        <li <?php if ($content == "log") echo 'class="active"'; ?>>
          <a href="index.php?content=log"><span class='glyphicon glyphicon-list'> <?php echo t('views.logs.title') ?></span></a>
        </li>
        <li <?php if ($content == "events_index" || $content == "events_new") echo 'class="active"'; ?>>
          <a href="index.php?content=events_index"><span class='glyphicon glyphicon-tasks'> <?php echo t('views.events.title') ?></span></a>
        </li>
        <li <?php if ($content == "user_shifts_sum_by_event") echo 'class="active"'; ?>>
          <a href="index.php?content=user_shifts_sum_by_event"><span class='glyphicon glyphicon-time'> <?php echo t('views.user_shifts.sum_report.title') ?></span></a>
        </li>
<?php
  }
}
?>
        <li <?php if ($content == "calendar") echo 'class="active"'; ?>>
          <a href="index.php?content=calendar"><span class='glyphicon glyphicon-calendar'> <?php echo t('views.user_shifts.title') ?></span></a>
        </li>
        <li <?php if ($content == "users_edit") echo 'class="active"'; ?>>
          <a href="index.php?content=users_edit"><span class='glyphicon glyphicon-user'> <?php echo t('views.users.user_profile') ?></span></a>
        </li>
        <li>
          <a href="index.php?content=users_logout"><span class='glyphicon glyphicon-log-out'> <?php echo t('logout_button') ?></span></a>
        </li>
      </ul>
    </div>
  </div>
</nav>
