<?php

if (!isset($_GET["username"])) {
  header("Location: index.php");
  die();
} else {
  $username = $_GET["username"];
}
$user = Users::find_by_username($username);
if (!$user) {
  header("Location: index.php");
  die();
} else {
  if (Users::password_reset($user)) {
    LogEntries::create("[request_password_reset] User '{$user['full_name']}'(email = '{$user['email']}') requested a password reset.");
  }
}
?>
<div class="whitebg-full">
  <div class="alert alert-info halfwidth centered">
    <?php echo t('views.login.password_reset.notice_message') ?>
  </div>
  <br>
  <div class="halfwidth centered">
    <a class="btn btn-lg btn-primary" href="index.php"><?php echo t('ok') ?></a>
  </div>
</div>
