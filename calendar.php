<?php

if (!isset($_SESSION['user_id'])) {
  header("Location: index.php");
  die();
}

$user_id = $_SESSION['user_id'];

if (isset($_GET["month"])) {
  $month = (int) $_GET["month"];
} else {
  $month = (int) date('n');
}

if (isset($_GET["year"])) {
  $year = (int) $_GET["year"];
} else {
  $year = (int) date('Y');
}

// Start date of current month and start date of next month so we loop through all dates in between
$first_day_of_selected_month = mktime(0, 0, 0, $month, 1, $year);
$first_day_of_previous_month = strtotime('-1 month', $first_day_of_selected_month);
$first_day_of_next_month = strtotime('+1 month', $first_day_of_selected_month);
$today = mktime(0, 0, 0, date('n'), date('j'), date('Y'));
$five_days_after_today = strtotime('+5 days', $today);
$previous_month = date('n', $first_day_of_previous_month);
$next_month = date('n', $first_day_of_next_month);
$previous_months_year = date('Y', $first_day_of_previous_month);
$next_months_year = date('Y', $first_day_of_next_month);

$month_name = t('month.' . $month);

if (isset($_POST["submit"])) {
  $new_shifts = [];
  if (!empty($_POST['shifts'])) {
    $shift_ids = array_keys($_POST['shifts']);
    foreach ($shift_ids as $shift_id) {
      $new_shifts[$shift_id] = true;
    }
  }

  $old_shifts = [];
  $user_shifts = UserShifts::all_for_user_in_date_range($user_id, date('Y-m-d', $first_day_of_selected_month), date('Y-m-d', $first_day_of_next_month));
  if (!empty($user_shifts)) {
    foreach ($user_shifts as $user_shift) {
      $old_shifts[$user_shift["shift_id"]] = true;
    }
  }

  $affected_shifts = array_merge(array_keys($new_shifts), array_keys($old_shifts));

  $shifts_to_remove = [];
  $shifts_to_add = [];
  foreach ($affected_shifts as $shift_id) {
    if (isset($new_shifts[$shift_id]) && $new_shifts[$shift_id] && !isset($old_shifts[$shift_id])) {
      $new_user_shift = [
        "user_id" => $user_id,
        "shift_id" => $shift_id
      ];
      array_push($shifts_to_add, $new_user_shift);
    } elseif (isset($old_shifts[$shift_id]) && $old_shifts[$shift_id] && !isset($new_shifts[$shift_id])) {
        array_push($shifts_to_remove, $shift_id);
    }
  }

  if (!empty($shifts_to_add) &&
      UserShifts::create_multiple($shifts_to_add) &&
      !empty($shifts_to_remove) &&
      UserShifts::delete_multiple_by_user_and_shift_ids($user_id, $shifts_to_remove)) {
    LogEntries::create("[change_shifts] User '{$_SESSION['username']}' changed his shifts.");
  }
}
?>
<div class="whitebg-full">
  <a type="button" class="btn btn-default btn-sm pull-left" href="?content=calendar&month=<?php echo $previous_month; ?>&year=<?php echo $previous_months_year; ?>"><span class="glyphicon glyphicon-arrow-left"></span> <?php echo t('views.user_shifts.monthly_view.previous_month_button') ?></a>
  <a type="button" class="btn btn-default btn-sm pull-right" href="?content=calendar&month=<?php echo $next_month; ?>&year=<?php echo $next_months_year; ?>"><?php echo t('views.user_shifts.monthly_view.next_month_button') ?> <span class="glyphicon glyphicon-arrow-right"></span></a>
  <div class="text-center">
    <h2><?php echo $month_name . " " . $year; ?></h2>
  </div>
  <form role="form" action="" method="POST">
    <table class="table table-bordered table-unfloat">
      <thead>
        <tr>

<?php
foreach (range(1, 7) as $day_of_week) {
?>
          <th><b><?php echo t('day_of_week.' . $day_of_week) ?></b></th>
<?php
}
?>
        </tr>
      </thead>
      <tbody>
        <tr>
<?php
// Fill empty days until the 1st of the month, if any
// N => 1 -> Mon, ..., 7 -> Sun
// Move it to 0 -> Mon, ..., 6 -> Sun by substracting 1
$weekday_of_first = (int)(date('N', $first_day_of_selected_month)) - 1;
for ($j = 0; $j < $weekday_of_first; $j++) {
?>
          <td>&nbsp;</td>
<?php
}
for ($day_of_week = $weekday_of_first, $current_date = $first_day_of_selected_month;
     $current_date < $first_day_of_next_month;
     $day_of_week++, $current_date = strtotime('+1 day', $current_date)) {
  $current_date_year = (int) date('Y', $current_date);
  $current_date_month = (int) date('n', $current_date);
  $current_date_day = (int) date('j', $current_date);
  // Start new row on Monday, except if the month starts on Monday
  // TODO: Add a user setting for first day of the week
  // Also keep in mind the empty cells before 1st and after last day
  if ($day_of_week % 7 == 0 && $day_of_week != 0) {
?>
        </tr>
        <tr>
<?php
  }
  $applied_percentage = Shifts::get_criticalness_for_date($current_date);
  $day_class = "";
  if ($current_date == $today && $applied_percentage < 75) {
    $day_class = 'class="danger"';
  }
  if ($current_date > $today && $current_date <= $five_days_after_today) {
    if ($applied_percentage < 25) {
      $day_class = 'class="danger"';
    } elseif ($applied_percentage < 75) {
      $day_class = 'class="warning"';
    }
  }
?>
          <td <?php echo $day_class ?>>
            <a href=index.php?content=day&year=<?php echo $current_date_year ?>&month=<?php echo $current_date_month ?>&day=<?php echo $current_date_day ?>><?php echo $current_date_day ?></a>
<?php
  $shifts_info = Shifts::get_shifits_info_for_date_and_user($current_date, $user_id);
  if (empty($shifts_info)) {
?>
            <br/>
            <br/>
            &nbsp;
<?php
  } else {
    $event_name = "";
    foreach ($shifts_info as $shift_info) {
      if ($event_name != $shift_info['event_name']) {
        $event_name = $shift_info['event_name'];
?>
              <br/>
              <strong><?php echo $event_name ?></strong>
<?php
      }
      $shift_id = $shift_info["id"];
      $people_needed = $shift_info["people_needed"];
      $people_applied = $shift_info["people_applied"];
      $shift_start_time = strftime('%H:%M', strtotime($shift_info["start_time"]));
      $shift_end_time = strftime('%H:%M', strtotime($shift_info["end_time"]));
      $user_applied = $shift_info['user_applied'];
      $max_people_applied = $people_applied >= $people_needed;
      $shift_label = "{$shift_start_time} - {$shift_end_time} ({$people_applied}/{$people_needed})";
      $checked_status = ($user_applied) ? 'checked="yes"' : '';
      $enabled = (!$applied_to_shift && $max_people_applied) ? 'disabled' : '';
      if (strtotime('+1 day', $current_date) > $today) {
?>
              <div class="checkbox">
                <input type="checkbox" id="shifts_<?php echo $shift_id ?>" name="shifts[<?php echo $shift_id ?>]" <?php echo "{$checked_status} {$enabled}" ?>/>
                <label for="shifts_<?php echo $shift_id ?>"><?php echo $shift_label ?></label>
              </div>
<?php
      } else {
?>
              <div>
                <p><span class="glyphicon <?php echo ($user_applied) ? 'glyphicon-ok' : 'glyphicon-unchecked' ?>"></span> <?php echo $shift_label ?></p>
              </div>
<?php
      }
    }
  }
?>
          </td>
<?php
}
// Fill empty days after last date in selected month until the end of the week
for ($j = $day_of_week; $j % 7 != 0; $j++) {
?>
          <td>&nbsp;</td>
<?php
}
?>
        </tr>
      <tbody>
    </table>
    <input class="btn btn-lg btn-primary btn-block" type="submit" id="submit" name="submit" value="<?php echo t('views.user_shifts.monthly_view.save_changes_button') ?>"/>
  </form>
</div>
