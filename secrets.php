<?php

$_secrets = [
  "password_salt" => "abcdefhijklmnopqrstuvwxyz",
  "hostname" => "localhost",
  "username" => "postgres",
  "password" => "",
  "database" => "vms",
  "port" => 5432,
  "host_address" => "http://vms-app.example.com/",
  "from_email" => "noreply@vms-app.example.com",
  "reply_to_email" => "noreply@vms-app.example.com"
];
