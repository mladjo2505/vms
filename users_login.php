<?php

function get_user_ip_address(){
  $ip = "";
  if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
    $ip = $_SERVER['HTTP_CLIENT_IP'];
  } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
    $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
  } else {
    $ip = $_SERVER['REMOTE_ADDR'];
  }
  return $ip;
}

$wrong_credentials = false;

if (isset($_POST["submit"])) {
  $username = clean($_POST["username"]);
  $password = clean($_POST["password"]);
  $user = Users::find_by_username($username);

  if (!$user) {
    $wrong_credentials = true;
  } else {
    $password_hash = md5($password . $_secrets['password_salt']);
    if ($user['password_hash'] != $password_hash) {
      $wrong_credentials = true;
      // TODO: Add ip ban for 1 hour after 3 failed attempts for the same user
      $ip = get_user_ip_address();
      LogEntries::create("Somebody tried to login as '{$username}' using an incorrect password from the following IP '{$ip}'.");
    } else {
      $_SESSION['username'] = $user['email'];
      $_SESSION['user_id'] = $user['id'];
      $_SESSION['user_type'] = $user['type'];
      LogEntries::create("[login] User '{$user['full_name']}' (email = '{$user['email']}') has logged in.");
      header("Location: index.php");
      die();
    }
  }
}

?>
<form id="login_form" class="form-signin" role="form" method="POST" action="" onsubmit="return validateLoginForm()">
  <div class="loginlogo center-block">
    <img src="img/logo.png"/>
  </div>
  <h2 class="form-signin-heading text-center"><?php echo t('views.login.title') ?></h2>
  <br/>
  <input class="form-control" type="username" autofocus="" placeholder="<?php echo t('views.login.placeholder.email') ?>" id="username" name="username"/>
  <input class="form-control" type="password" placeholder="<?php echo t('views.login.placeholder.password') ?>" id="password" name="password"/>
  <input class="btn btn-lg btn-primary btn-block" type="submit" id="submit" name="submit" value="<?php echo t('views.login.login_button') ?>"/>
  <br/>
<?php
if ($wrong_credentials) {
?>
  <div class="alert alert-warning">
<?php
  echo t('views.login.error_message.login_error');
?>
    <br/>
    <a href="index.php?content=users_password_reset&username=<?php echo $username ?>"><?php echo t('views.login.forgot_password') ?></a>
  </div>
<?php
}
?>
</form>
