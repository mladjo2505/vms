<?php

if (!isset($_SESSION['user_id']) || !(isset($_SESSION['user_type']) && $_SESSION['user_type'] < 3)) {
  header("Location: index.php");
  die();
}

if (isset($_GET["shift_id"])) {
  $shift_id = $_GET["shift_id"];
}
if (isset($_GET["user_id"])) {
  $user_id = $_GET["user_id"];
} else {
  $user_id = false;
}
if (isset($_GET["search_name"])) {
  $search_name = $_GET["search_name"];
} else {
  $search_name = false;
}
if (!$search_name && !$user_id) {
?>
<div class='container whitebg'>
  <h2><?php echo t('views.user_shifts.new_form.form_title') ?></h2>
  <form class='form-group' method='GET' action=''>
    <input id='content' name='content' type='hidden' value='user_shifts_new' />
    <input id='shift_id' name='shift_id' type='hidden' value='<?php echo $shift_id; ?>' />
    <label for='search_name'><?php echo t('views.user_shifts.new_form.name') ?></label>
    <input class='form-control' id='search_name' name='search_name' type='text' required="" placeholder="<?php echo t('views.user_shifts.new_form.placeholder.name') ?>" />
    <br/>
    <input class='btn btn-sm btn-primary btn-block' id='submit' name='submit' type='submit' value='<?php echo t('views.user_shifts.new_form.search_button') ?>'/>
  </form>
</div>
<?php
} else {
  if ($user_id) {
?>
<?php
    $new_user_shift = [
      "shift_id" => $shift_id,
      "user_id" => $user_id
    ];
    if (UserShifts::create($new_user_shift)) {
      $shift = Shifts::find_by_id($shift_id);
      $shift_date = strtotime($shift['date']);
      $year = (int)(date('Y', $shift_date));
      $month = (int)(date('n', $shift_date));
      $day = (int)(date('j', $shift_date));
      LogEntries::create("[manual_add_user_shift] User '{$_SESSION['username']}' manualy added a shift (shift_id = '{$shift_id}', user_id = '{$user_id}').");
      header("Location: /index.php?content=day&year={$year}&month={$month}&day={$day}");
      die();
    }
  } elseif ($users = Users::find_by_full_name($search_name)) {
?>
<div class='tablecontainer whitebg-full text-center'>
  <h2><?php echo t('views.user_shifts.new_form.search_results_for') . " '{$search_name}'" ?></h2>
  <table class='table table-striped table-bordered table-condensed'>
    <thead>
      <tr>
        <th><b><?php echo t('views.user_shifts.new_form.full_name') ?></b></td>
        <th><b><?php echo t('views.user_shifts.new_form.telephone') ?></b></td>
        <th><b><?php echo t('views.user_shifts.new_form.email') ?></b></td>
        <th><b><?php echo t('views.user_shifts.new_form.occupation') ?></b></td>
        <th><b><?php echo t('views.user_shifts.new_form.personal_id_number') ?></b></td>
        <th><b><?php echo t('views.user_shifts.new_form.address') ?></b></td>
        <th><b><?php echo t('views.user_shifts.new_form.action') ?></b></td>
      </tr>
    </thead>
    <tbody>
<?php
    foreach ($users as $user) {
?>
      <tr>
        <td><b><?php echo $user['full_name'] ?></b></td>
        <td><b><?php echo $user['telephone'] ?></b></td>
        <td><b><?php echo $user['email'] ?></b></td>
        <td><b><?php echo $user['occupation'] ?></b></td>
        <td><b><?php echo $user['personal_id_number'] ?></b></td>
        <td><b><?php echo $user['address'] ?></b></td>
        <td><a type="button" class="btn btn-default btn-xs" href="index.php?content=user_shifts_new&shift_id=<?php echo $shift_id ?>&user_id=<?php echo $user['id'] ?>"><span class="glyphicon glyphicon-plus"> <?php echo t('views.user_shifts.new_form.add_button') ?></span></a></td>
      </tr>
<?php
    }
?>
    </tbody>
  </table>
</div>
<?php
  }
}
