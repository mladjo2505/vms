<?php

function clean($string) {
    $string = @trim($string);
    if(get_magic_quotes_gpc()) {
        $string = addslashes($string);
    }
    return pg_escape_string($string);
}

require 'secrets.php';
$hostname = $_secrets['hostname'];
$username = $_secrets['username'];
$password = $_secrets['password'];
$database = $_secrets['database'];
$port = $_secrets['port'];

$connection = pg_connect("host={$hostname} port={$port} dbname={$database} user={$username} password={$password}");
if (!$connection) {
    die("Error establishing a connection with the database server.");
}

