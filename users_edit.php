<?php

if (!isset($_SESSION['user_id'])) {
  header("Location: index.php");
  die();
}

$wrong_username = false;
$wrong_password = false;
$wrong_password_confirmation = false;
$success = false;

$user_id = $_SESSION['user_id'];
$user = Users::find_by_id($user_id);

if (isset($_POST["submit"])) {
  $current_password = clean($_POST["password"]);
  $current_password_hash = md5($current_password . $_secrets['password_salt']);
  $updated_user = [];
  if ($current_password_hash != $user['password_hash']) {
    $wrong_password = true;
  } else {
    $updated_username = clean($_POST["username"]);
    $updated_user['full_name'] = clean($_POST["full_name"]);
    $updated_user['occupation'] = clean($_POST["occupation"]);
    $updated_user['telephone'] = clean($_POST["telephone"]);
    $updated_user['personal_id_number'] = clean($_POST["personal_id_number"]);
    $updated_user['address'] = clean($_POST["address"]);
    if ($user['email'] != updated_username) {
      if (Users::find_by_email($updated_username)) {
        $wrong_username = true;
      } else {
        $updated_user['email'] = $updated_username;
      }
    }
    if (isset($_POST["change_password"])) {
      $new_password = clean($_POST["new_password"]);
      $new_password_confirmation = clean($_POST["new_password_confirmation"]);
      if ($new_password != $new_password_confirmation) {
        $wrong_password_confirmation = true;
      } else {
        $updated_user['password_hash'] = md5($new_password . $_secrets['password_salt']);
      }
    }
  }
  if (!$wrong_password && !$wrong_username && !$wrong_password_confirmation) {
    Users::update($user_id, $updated_user);
    $user = Users::find_by_id($user_id);
    LogEntries::create("[edit_user_info] User '{$_SESSION['username']}' changed profile info (email = '{$username}', full_name = '{$full_name}').");
    $_SESSION['username'] = $user['email'];
    $success = true;
    header("Location: index.php?content=users_edit");
    die();
  }
}
?>
<div class="container whitebg-mid">
  <form class="form-other form-horizontal" role="form" method="POST" action="" onsubmit="return checkEditUserForm()">
    <h2 class="text-center"><?php echo t('views.users.edit_form.form_title') ?></h2>
    <br/>
    <div class="form-group form-group-top">
      <label for="username" class="col-sm-4 control-label"><?php echo t('views.users.edit_form.email') ?></label>
      <div class="col-sm-8">
        <input class="form-control form-control-top" type="username" required="" id="username" name="username" value="<?php echo $user["email"]?>"/>
      </div>
    </div>
    <div class="form-group form-group-top form-group-bottom">
      <label for="full_name" class="col-sm-4 control-label"><?php echo t('views.users.edit_form.full_name') ?></label>
      <div class="col-sm-8">
        <input class="form-control form-control-top form-control-bottom" type="text" required="" id="full_name" name="full_name" value="<?php echo $user["full_name"]?>"/>
      </div>
    </div>
    <div class="form-group form-group-top form-group-bottom">
      <label for="occupation" class="col-sm-4 control-label"><?php echo t('views.users.edit_form.occupation') ?></label>
      <div class="col-sm-8">
        <input class="form-control form-control-top form-control-bottom" type="text" required="" id="occupation" name="occupation" value="<?php echo $user["occupation"]?>"/>
      </div>
    </div>
    <div class="form-group form-group-top form-group-bottom">
      <label for="telephone" class="col-sm-4 control-label"><?php echo t('views.users.edit_form.telephone') ?></label>
      <div class="col-sm-8">
        <input class="form-control form-control-top form-control-bottom" type="text" required="" id="telephone" name="telephone" value="<?php echo $user["telephone"]?>"/>
      </div>
    </div>
    <div class="form-group form-group-top form-group-bottom">
      <label for="personal_id_number" class="col-sm-4 control-label"><?php echo t('views.users.edit_form.personal_id_number') ?></label>
      <div class="col-sm-8">
        <input class="form-control form-control-top form-control-bottom" type="text" required="" id="personal_id_number" name="personal_id_number" value="<?php echo $user["personal_id_number"]?>"/>
      </div>
    </div>
    <div class="form-group form-group-bottom">
      <label for="address" class="col-sm-4 control-label"><?php echo t('views.users.edit_form.address') ?></label>
      <div class="col-sm-8">
        <input class="form-control form-control-bottom" type="text" required="" id="address" name="address" value="<?php echo $user["address"]?>"/>
      </div>
    </div>
    <div class="checkbox">
      <input id="change_password" name="change_password" type="checkbox" value="1" onclick="toggleNewPasswordFields()"/>
      <label for="change_password"><?php echo t('views.users.edit_form.set_new_password') ?></label>
    </div>
    <br/>
    <input class="form-control form-control-top" type="password" placeholder="<?php echo t('views.users.edit_form.placeholder.new_password') ?>" id="new_password" name="new_password" disabled/>
    <input class="form-control form-control-bottom" type="password" placeholder="<?php echo t('views.users.edit_form.placeholder.new_password_confirmation') ?>" id="new_password_confirmation" name="new_password_confirmation" disabled/>
    <br/>
    <label class="alert alert-warning infobox fullwidth" for="password">
      <span class='glyphicon glyphicon-warning-sign'></span> <?php echo t('views.users.edit_form.change_info_warning') ?>
    </label>
    <input class="form-control" type="password" placeholder="<?php echo t('views.users.edit_form.placeholder.current_password') ?>" id="password" name="password"/>
    <br/>
    <input class="btn btn-lg btn-primary btn-block" type="submit" id="submit" name="submit" value="<?php echo t('views.users.edit_form.save_changes_button') ?>"/>
    <br/>
    <div class="alert alert-warning">
<?php
if ($wrong_password_confirmation) {
  echo t('views.users.error_message.password_confirmation_mismatch');
} elseif ($wrong_username) {
  echo t('views.users.error_message.wrong_username');
} elseif ($wrong_password) {
  echo t('views.users.error_message.wrong_password');
} elseif ($success) {
  echo t('views.users.edit_form.success_notice');
} else {
  echo t('views.users.edit_form.edit_info');
}
?>
    </div>
  </form>
</div>
