<?php

if (!isset($locale)) {
  $locale = "en";
}
$translations = [];
foreach (glob("translations/*.json") as $filename) {
  $lang = basename($filename, ".json");
  $translations[$lang] = json_decode(file_get_contents($filename), true);
}

function t($coordinates) {
    global $translations, $locale;

    $coordinates_array = explode('.', $coordinates);
    $translation = $translations[$locale];
    foreach ($coordinates_array as $current) {
        if (!is_null($translation) && is_array($translation) && array_key_exists($current, $translation)) {
            $translation = $translation[$current];
        } else {
            $translation = "Translation missing for coordinates '${coordinates}'";
            break;
        }
    }
    if (!is_string($translation)) {
        return "Translation missing for coordinates '${coordinates}'";
    }
    return $translation;
}

