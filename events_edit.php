<?php

if (isset($_GET["id"])) {
  $id = $_GET["id"];
}

if ($id && (!isset($_SESSION['user_id']) || !(isset($_SESSION['user_type']) && $_SESSION['user_type'] == 1))) {
  header("Location: index.php");
  die();
}

$event = Events::find_by_id($id);
if (isset($_POST["submit"])) {
  $updated_event = [];
  $updated_event['name'] = clean($_POST["name"]);
  $updated_event['description'] = clean($_POST["description"]);
  $updated_event['start_date'] = clean($_POST["start_date"]);
  $updated_event['end_date'] = clean($_POST["end_date"]);
  if (Events::update($id, $updated_event)) {
    LogEntries::create("[update_event] User '{$_SESSION['username']}' changed event (id = {$id}).");
    header("Location: index.php?content=events_index");
    die();
  }
}

if (!$event) {
  header("Location: index.php?content=events_index");
  die();
}
?>
<div class="container whitebg">
  <form class="form-other" role="form" method="POST" action="">
    <h2><?php echo t('views.events.edit_form.form_title') ?></h2>
    <br/>
    <input class="form-control form-control-top" type="text" required="" value="<?php echo $event['name']; ?>" id="name" name="name"/>
    <input class="form-control form-control-top form-control-bottom picker" type="text" required="" value="<?php echo $event['start_date']; ?>" id="start_date" name="start_date"/>
    <input class="form-control form-control-bottom picker" type="text" required="" value="<?php echo $event['end_date']; ?>" id="end_date" name="end_date"/>
    <input class="form-control form-control-top form-control-bottom" type="text" required="" value="<?php echo $event['description']; ?>" id="description" name="description"/>
    <br/>
    <input class="btn btn-lg btn-primary btn-block" type="submit" id="submit" name="submit" value="<?php echo t('views.events.edit_form.save_changes_button'); ?>"/>
    <br/>
    <div class="alert alert-warning">
      <?php echo t('views.events.edit_form.form_info') ?>
    </div>
  </form>
  <script type="text/javascript">
    $("#start_date").datepicker( { format: "yyyy-mm-dd", weekStart: 1 } );
    $("#end_date").datepicker( { format: "yyyy-mm-dd", weekStart: 1 } );
  </script>
</div>
