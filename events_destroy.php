<?php

if (!isset($_SESSION['user_id']) || !(isset($_SESSION['user_type']) && $_SESSION['user_type'] == 1)) {
  header("Location: index.php");
  die();
}

$event_id = clean($_GET['id']);
if (Events::delete($event_id)) {
  LogEntries::create("[remove_event] User '{$_SESSION['username']}' deleted event (id = {$event_id}).");
  header("Location: index.php?content=events_index");
  die();
}
