<?php

if (!isset($_SESSION['user_id'])) {
  header("Location: index.php");
  die();
}

$user_id = $_SESSION['user_id'];

if (isset($_GET["day"])) {
  $day = (int) $_GET["day"];
} else {
  $day = (int) date('j');
}

if (isset($_GET["month"])) {
  $month = (int) $_GET["month"];
} else {
  $month = (int) date('n');
}

if (isset($_GET["year"])) {
  $year = (int) $_GET["year"];
} else {
  $year = (int) date('Y');
}

$selected_date = mktime(0, 0, 0, $month, $day, $year);
$selected_date_string = date('Y-m-d', $selected_date);
$previous_date = strtotime('-1 day', $selected_date);
$next_date = strtotime('+1 day', $selected_date);
$previous_dates_day = date('j', $previous_date);
$next_dates_day = date('j', $next_date);
$previous_dates_month = date('n', $previous_date);
$next_dates_month = date('n', $next_date);
$previous_dates_year = date('Y', $previous_date);
$next_dates_year = date('Y', $next_date);

$month_name = t('month.' . $month);
$day_name = t('day_of_week.' . date('N', $selected_date));
?>
<div class="container whitebg-full">
  <a type="button" class="btn btn-default btn-sm pull-left" href="?content=day&year=<?php echo $previous_dates_year; ?>&month=<?php echo $previous_dates_month; ?>&day=<?php echo $previous_dates_day; ?>"><span class="glyphicon glyphicon-arrow-left"> <?php echo t('views.user_shifts.daily_view.previous_day_button') ?></span></a>
  <a type="button" class="btn btn-default btn-sm pull-right" href="?content=day&year=<?php echo $next_dates_year; ?>&month=<?php echo $next_dates_month; ?>&day=<?php echo $next_dates_day; ?>"><?php echo t('views.user_shifts.daily_view.next_day_button') ?> <span class="glyphicon glyphicon-arrow-right"></span></a>
  <div class="text-center">
    <a type="button" class="btn btn-default btn-sm" href="?content=calendar&year=<?php echo $year; ?>&month=<?php echo $month; ?>"><span class="glyphicon glyphicon-calendar"></span> <?php echo t('views.user_shifts.daily_view.switch_to_monthly_view_for_button') . " {$month_name} {$year}" ?></a>
    <h2><?php echo "{$day_name}, {$day} {$month_name} {$year}" ?></h2>
  </div>
<?php
$shifts = Shifts::all_for_date($selected_date_string);
if (empty($shifts)) {
?>
  <div class="alert alert-info">
    <?php echo t('views.user_shifts.daily_view.no_shifts_for_day_notice') ?>
  </div>
<?php
} else {
  $applied_to = [];
  $applied_to_shifts = UserShifts::all_for_date($user_id, $selected_date_string);
  if ($applied_to_shifts) {
    foreach ($applied_to_shifts as $applied_shift) {
      $applied_to[$applied_shift["shift_id"]] = $applied_shift["id"];
    }
  }
  $current_event_id = null;
  foreach ($shifts as $shift) {
    $shift_id = $shift['id'];
    $event_id = $shift['event_id'];
    if ($event_id != $current_event_id) {
      $current_event_id = $event_id;
      $event = Events::find_by_id($current_event_id);
      $event_name = $event['name'];
?>
  <h1><?php echo $event_name ?></h1>
<?php
    }
?>
  <h4><?php echo strftime('%H:%M', strtotime($shift['start_time'])) . ' &ndash; ' . strftime('%H:%M', strtotime($shift['end_time'])) ?></h4>
<?php
    if ($_SESSION['user_type'] == 1) {
?>
  <a class="btn btn-default btn-xs" href="index.php?content=shifts_edit&shift_id=<?php echo $shift_id ?>"><span class="glyphicon glyphicon-edit"></span> <?php echo t('views.user_shifts.daily_view.edit_shift_button') ?></a>
  <a class="btn btn-default btn-xs" href="index.php?content=shifts_destroy&shift_id=<?php echo $shift_id ?>"><span class="glyphicon glyphicon-trash"></span> <?php echo t('views.user_shifts.daily_view.delete_shift_button') ?></a>
<?php
    }
    if (isset($applied_to[$shift_id]) && $applied_to[$shift_id]) {
?>
  <a class="btn btn-default btn-xs" href="index.php?content=user_shifts_destroy&user_shift_id=<?php echo $applied_to[$shift_id] ?>"><span class="glyphicon glyphicon-minus"></span> <?php echo t('views.user_shifts.daily_view.abandon_shift_button') ?></a>
<?php
    } else {
?>
  <a class="btn btn-default btn-xs" href="index.php?content=user_shifts_new&shift_id=<?php echo $shift_id ?>&user_id=<?php echo $user_id ?>"><span class="glyphicon glyphicon-plus"></span> <?php echo t('views.user_shifts.daily_view.apply_to_shift_button') ?></a>
<?php
    }
    if ($_SESSION['user_type'] < 3) {
          ?>
      <a type="button" class="btn btn-default btn-xs" href="index.php?content=user_shifts_new&shift_id=<?php echo $shift_id ?>"><span class="glyphicon glyphicon-plus"></span> <?php echo t('views.user_shifts.daily_view.add_user_shift_button') ?></a>
<?php
    }
?>
  <br/>
  <br/>
<?php
    $user_shifts = UserShifts::all_for_shift($shift_id);
    if (empty($user_shifts)) {
?>
  <div class="alert alert-info">
    <?php echo t('views.user_shifts.daily_view.nobody_applied_for_shift_notice') ?>
  </div>
<?php
    } else {
?>
  <table class="table table-striped table-bordered">
    <tbody>
<?php
      foreach ($user_shifts as $user_shift) {
?>
      <tr>
        <td>
<?php
        $user = Users::find_by_id($user_shift['user_id']);
        echo "{$user['full_name']}, {$user['telephone']}, {$user['email']}, {$user['occupation']}";
        if ($_SESSION['user_type'] < 3) {
?>
          <a style="float: right;" type="button" class="btn btn-default btn-xs" href="index.php?content=user_shifts_destroy&user_shift_id=<?php echo $user_shift['id'] ?>"><span class="glyphicon glyphicon-trash"></span> <?php echo t('views.user_shifts.daily_view.remove_user_shift_button') ?></a>
<?php
        }
?>
        </td>
      </tr>
<?php
      }
    }
?>
    </tbody>
  </table>
<?php
  }
}
?>
</div>
