<?php

$user_is_admin = $_SESSION['user_type'] < 2;

$notices = Notices::last();
if (empty($notices)) {
?>
  <div class="alert alert-info">
    <?php echo t('views.notices.empty_message') ?>
  </div>
<?php
} else {
  foreach ($notices as $notice) {
?>
  <div class="alert alert-info">
    <h3><?php echo $notice["title"] ?></h3>
    <p><?php echo $notice["contents"] ?></p>
<?php
    if ($user_is_admin) {
?>
    <br/>
    <a class="btn btn-default btn-xs" href="index.php?content=notices_destroy&id=<?php echo $notice["id"] ?>">
      <span class='glyphicon glyphicon-trash'> <?php echo t('views.notices.remove_button') ?></span>
    </a>
<?php
    }
?>
  </div>
<?php
  }
}
