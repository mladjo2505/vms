<?php

if (!isset($_SESSION['user_id'])) {
  header("Location: index.php");
  die();
}
$user_can_add_notice = $_SESSION['user_type'] < 3;
?>
<div class="container whitebg">
  <div class="text-center">
    <h2><?php echo t('views.notices.title') ?></h2>
  </div>
<?php
if ($user_can_add_notice && isset($_POST["submit"])) {
  $title = clean($_POST["title"]);
  $contents = clean(nl2br($_POST["contents"]));
  $user_id = clean($_SESSION['user_id']);
  if (Notices::create($title, $contents, $user_id)) {
    LogEntries::create("[add_notice] User '{$_SESSION['username']}' added a notice (title = '{$title}').");
  }
}
if ($user_can_add_notice) {
?>
  <form class="form-other form-horizontal" role="form" method="POST" action="">
    <input class="form-control" type="text" required="" placeholder="<?php echo t('views.notices.new_form.title') ?>" id="title" name="title"/>
    <br/>
    <textarea class="form-control" rows="3" placeholder="<?php echo t('views.notices.new_form.content') ?>" id="contents" name="contents"></textarea>
    <br/>
    <input class="btn btn-lg btn-primary btn-block" type="submit" id="submit" name="submit" value="<?php echo t('views.notices.new_form.add_button') ?>"/>
    <br/>
  </form>
<?php
}
require 'notices_recent.php';
?>
</div>
