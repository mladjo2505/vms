function validateLoginForm() {
    const username = document.getElementById('username').value;
    const password = document.getElementById('password').value;
    const login_form = document.getElementById('login_form');
    const blank_form_warning = document.getElementById('blank_form_warning');
    if (username === '' || password === '') {
        if (!blank_form_warning) {
            // TODO: Fix to include a way to load a translation
            login_form.innerHTML += '<div id="blank_form_warning" class="alert alert-warning">Please fill in email and password fields to log in.</div>';
        }
        return false;
    }
    return true;
}

function validateLostPassForm() {
    const password = document.getElementById('password').value;
    const warning_message = document.getElementById('warning_message');
    if (password == '') {
        // TODO: Fix to include a way to load a translation
        warning_message.innerHTML = "New password can't be blank!";
        return false;
    }
    return true;
}

function validateNewUserForm() {
    const username = document.getElementById('username').value;
    const username_confirmation = document.getElementById('username_confirmation').value;
    const password = document.getElementById('password').value;
    const password_confirmation = document.getElementById('password_confirmation').value;
    const full_name = document.getElementById('full_name').value;
    const occupation = document.getElementById('occupation').value;
    const telephone = document.getElementById('telephone').value;
    const warning_message = document.getElementById('warning_message');
    if (username == '' || password == '' || password_confirmation == '' || full_name == '' || occupation == '' || telephone == '') {
        // TODO: Fix to include a way to load a translation
        warning_message.innerHTML = 'Please fill out all the fields.';
        return false;
    }
    if (password !== password_confirmation) {
        // TODO: Fix to include a way to load a translation
        //this is an instance of 'login.wrong_password_confirmation' key
        warning_message.innerHTML = "Password and password confirmation don't match.";
        return false;
    }
    if (username !== username_confirmation) {
        // TODO: Fix to include a way to load a translation
        warning_message.innerHTML = "Email and email confirmation don't match.";
        return false;
    }
    return true;
}

function toggleNewPasswordFields() {
    const new_password = document.getElementById('new_password');
    const new_password_confirmation = document.getElementById('new_password_confirmation');
    new_password.disabled = !new_password.disabled;
    new_password_confirmation.disabled = !new_password_confirmation.disabled;
}

// TODO: Add validation for new event form
function validateNewEventForm() {
    return true;
}

// TODO: Add validation for edit event form
function validateEditEventForm() {
    return true;
}

// TODO: Add validation for new shift form
function validateNewShiftForm() {
    return true;
}
// TODO: Add validation for edit shift form
function validateEditShiftForm() {
    return true;
}

// TODO: Add validation for new notice form
function validateNewNoticeForm() {
    return true;
}
