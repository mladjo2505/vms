<?php

spl_autoload_register('autoloader');

function autoloader($class_name)
{
    include_once 'models/' . $class_name . '.php';
}
