<?php

if (isset($_GET["event_id"])) {
  $event_id = $_GET["event_id"];
}

if ($event_id && (!isset($_SESSION['user_id']) || !(isset($_SESSION['user_type']) && $_SESSION['user_type'] == 1))) {
  header("Location: index.php");
  die();
}

$event = Events::find_by_id($event_id);
$shifts = Shifts::all_for_event($event_id);
?>
<div class="whitebg-mid">
  <table class='table table-striped table-bordered'>
    <caption>
      <h3><?php echo t('views.shifts.table.table_title') . " {$event['name']}" ?></h3>
    </caption>
    <thead>
      <tr>
        <th><b><?php echo t('views.shifts.table.number_sign') ?></b></th>
        <th><b><?php echo t('views.shifts.table.date') ?></b></th>
        <th><b><?php echo t('views.shifts.table.start_time') ?></b></th>
        <th><b><?php echo t('views.shifts.table.end_time') ?></b></th>
        <th><b><?php echo t('views.shifts.table.duration') ?></b></th>
        <th><b><?php echo t('views.shifts.table.people_needed') ?></b></th>
        <th><b><?php echo t('views.shifts.table.action') ?></b></th>
      </tr>
    </thead>
    <tbody>
<?php
if (empty($shifts)) {
?>
      <tr>
        <td class="text-center" colspan="7"><?php echo t('views.shifts.table.empty_message') ?></td>
      </tr>
<?php
} else {
  foreach ($shifts as $shift) {
    $shift_start = strtotime("{$shift['date']} {$shift['start_time']}");
    $shift_end = strtotime("{$shift['date']} {$shift['end_time']}");
    $shift_duration = $shift_end - $shift_start;
    $hours = (int)(strftime('%H', $shift_duration));
    $minutes = (int)(strftime('%M', $shift_duration));
    $duration_string = "{$hours} " . t('views.shifts.table.hours');
    if ($minutes) {
      $duration_string .= " {$minutes} " . t('views.shifts.table.minutes');
    }
?>
      <tr>
        <td><?php echo $shift["id"]; ?></td>
        <td><?php echo $shift["date"]; ?></td>
        <td><?php echo strftime('%H:%M', strtotime($shift['start_time'])); ?></td>
        <td><?php echo strftime('%H:%M', strtotime($shift['end_time'])); ?></td>
        <td><?php echo $duration_string ?></td>
        <td><?php echo $shift["people_needed"]; ?></td>
        <td>
          <a class="btn btn-default btn-xs" href="index.php?content=shifts_destroy&shift_id=<?php echo $shift['id']; ?>"><span class="glyphicon glyphicon-trash"> <?php echo t('views.shifts.table.delete_button') ?></span></a>
          <a class="btn btn-default btn-xs" href="index.php?content=shifts_edit&shift_id=<?php echo $shift['id']; ?>"><span class="glyphicon glyphicon-edit"> <?php echo t('views.shifts.table.edit_button') ?></span></a>
        </td>
      </tr>
<?php
  }
}
?>
      <tr>
        <td class="text-left" colspan="8">
          <a class="btn btn-default btn-xs" href="index.php?content=shifts_new&event_id=<?php echo $event_id; ?>"><span class="glyphicon glyphicon-plus"> <?php echo t('views.shifts.table.add_new_button') ?></span></a>
        </td>
      </tr>
    </tbody>
  </table>
</div>
