<?php

$_secrets = [
  "password_salt" => "some salt phrase",
  "hostname" => "hostname",
  "username" => "username",
  "password" => "password",
  "database" => "database",
  "port" => 5432,
  "host_address" => "http://vms-app.example.com/",
  "from_email" => "noreply@vms-app.example.com",
  "reply_to_email" => "noreply@vms-app.example.com"
];
