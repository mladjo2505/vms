<div class="container whitebg-full">
<?php

if (!isset($_SESSION['user_id']) || $_SESSION['user_type'] > 1) {
  header("Location: index.php");
  die();
}

$log_entries = LogEntries::last();
?>
  <table class='table table-striped table-bordered'>
    <caption><h3><?php echo t('views.logs.table_caption') ?></h3></caption>
      <tr>
        <td><b><?php echo t('views.logs.time') ?></b></td>
        <td><b><?php echo t('views.logs.activity') ?></b></td>
      </tr>
<?php
if (empty($log_entries)) {
?>
      <tr>
        <td class='text-center' colspan='2'><?php echo t('views.logs.activity') ?></td>
      </tr>
<?php
} else {
  foreach ($log_entries as $log_entry) {
?>
      <tr>
        <td><?php echo $log_entry["created_at"] ?></td>
        <td><?php echo $log_entry["message"] ?></td>
      </tr>
<?php
  }
}
?>
  </table>
</div>
