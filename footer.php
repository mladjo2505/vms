<div id="footer">
  <div class="container">
    <p class="text-muted">
      <a href="https://gitlab.com/mladjo2505/vms"><img src="img/logo32.png" width="32" height="32" alt="VMS"/> <?php echo t('views.footer.text') ?></a>
    </p>
  </div>
</div>
