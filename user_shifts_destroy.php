<?php

if (!isset($_SESSION['user_id']) || !(isset($_SESSION['user_type']) && $_SESSION['user_type'] < 3)) {
  header("Location: index.php");
  die();
}

if (isset($_GET["user_shift_id"])) {
  $user_shift_id = $_GET["user_shift_id"];
}

$user_shift = UserShifts::find_by_id($user_shift_id);
if ($user_shift && ($user_shift['user_id'] == $_SESSION['user_id'] || $_SESSION['user_type'] == 1)) {
  $shift = Shifts::find_by_id($user_shift['shift_id']);
  $shift_date = strtotime($shift['date']);
  $year = (int)(date('Y', $shift_date));
  $month = (int)(date('n', $shift_date));
  $day = (int)(date('j', $shift_date));
  if (UserShifts::delete($user_shift_id)) {
    LogEntries::create("[manual_remove_shift] User '{$_SESSION['username']}' manually deleted a user shift (user_shift_id = '{$user_shift_id}').");
    header("Location: /index.php?content=day&year={$year}&month={$month}&day={$day}");
    die();
  }
}
