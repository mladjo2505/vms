<?php

if (!isset($_SESSION['user_id']) || !(isset($_SESSION['user_type']) && $_SESSION['user_type'] == 1)) {
  header("Location: index.php");
  die();
}

if (isset($_GET["shift_id"])) {
  $id = $_GET["shift_id"];
}
if ($id) {
  $shift = Shifts::find_by_id($id);
  if (Shifts::delete($id)) {
    LogEntries::create("[manual_remove_shift] User '{$_SESSION['username']}' manualy deleted a shift (id = '{$id}').");
    header("Location: index.php?content=shifts_index&event_id={$shift['event_id']}");
    die();
  }
}
