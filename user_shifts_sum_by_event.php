<?php

if (!isset($_SESSION['user_id']) || $_SESSION['user_type'] != 1) {
  header("Location: index.php");
  die();
}

$user_types = [
  'any',
  'administrator',
  'demonstrator',
  'volunteer'
];
$user_shifts_for_report = [];
$events = Events::all();
if (!empty($events)) {
  $last_event_id = end($events)['id'];
  $user_type = 3;

  if (isset($_POST["submit"])) {
    $event_id = clean($_POST['event_id']);
    $user_type = clean($_POST['user_type']);
    $user_shifts_for_report = Reports::user_shifts_sum($event_id, $user_type);
  }
  if (!isset($event_id)) {
    $event_id = $last_event_id;
  }
}
$table_class = (empty($events)) ? 'whitebg' : 'whitebg-full';
?>
<div class="tablecontainer <?php echo $table_class; ?>">
<?php
if (empty($events)) {
?>
  <div class="alert alert-info">
    <?php echo t('views.user_shifts.sum_report.table.no_events_notice') ?>
  </div>
<?php
} else {
?>
  <form class="form-other form-horizontal" role="form" method="POST" action="">
    <div class="form-group">
      <div class="col-xs-6">
        <select class="form-control" id="event_id" name="event_id">
<?php
  $user_type_selected = [
    $user_type == 0,
    $user_type == 1,
    $user_type == 2,
    $user_type == 3
  ];
  foreach ($events as $event) {
?>
          <option value="<?php echo $event['id'] ?>" <?php echo $event['id'] == $event_id ? 'selected' : '' ?>><?php echo $event['name'] ?></option>
<?php
  }
?>
        </select>
      </div>
      <div class="col-xs-6">
        <select class="form-control" id="user_type" name="user_type">
<?php
  for ($i = 0; $i < 4; $i++) {
?>
          <option value="<?php echo $i ?>" <?php echo $user_type_selected[$i] ? 'selected' : '' ?>><?php echo t('views.user_shifts.sum_report.form.user_types.' . $user_types[$i]) ?></option>
<?php
  }
?>
        </select>
      </div>
    </div>
    <div class="form-group">
      <div class="col-xs-12">
        <input class="btn btn-primary btn-block" type="submit" id="submit" name="submit" value="<?php echo t('views.user_shifts.sum_report.form.calculate_button') ?>"/>
      </div>
    </div>
  </form>
  <table class="table table-striped table-bordered table-condensed">
    <thead>
      <tr>
        <th><b><?php echo t('views.user_shifts.sum_report.table.number_sign') ?></b></th>
        <th><b><?php echo t('views.user_shifts.sum_report.table.full_name') ?></b></th>
        <th><b><?php echo t('views.user_shifts.sum_report.table.telephone') ?></b></th>
        <th><b><?php echo t('views.user_shifts.sum_report.table.email') ?></b></th>
        <th><b><?php echo t('views.user_shifts.sum_report.table.occupation') ?></b></th>
        <th><b><?php echo t('views.user_shifts.sum_report.table.personal_id_number') ?></b></th>
        <th><b><?php echo t('views.user_shifts.sum_report.table.address') ?></b></th>
        <th><b><?php echo t('views.user_shifts.sum_report.table.hours_shifts') ?></b></th>
      </tr>
    </thead>
    <tbody>
<?php
  if (empty($events)) {
?>
      <tr><td class="text-center" colspan="8"><?php echo t('views.user_shifts.sum_report.table.no_events_notice') ?></td></tr>
<?php
  } elseif (empty($user_shifts_for_report)) {
?>
      <tr><td class="text-center" colspan="8"><?php echo t('views.user_shifts.sum_report.table.no_results_notice') ?></td></tr>
<?php
  } else {
    foreach ($user_shifts_for_report as $user_shift) {
?>
      <tr>
        <td><?php echo $user_shift['user_id'] ?></td>
        <td><?php echo $user_shift['full_name'] ?></td>
        <td><?php echo $user_shift['telephone'] ?></td>
        <td><?php echo $user_shift['email'] ?></td>
        <td><?php echo $user_shift['occupation'] ?></td>
        <td><?php echo $user_shift['personal_id_number'] ?></td>
        <td><?php echo $user_shift['address'] ?></td>
        <td><?php echo "{$user_shift['hours']} " . t('views.user_shifts.sum_report.table.hours') . " / " . " {$user_shift['shifts']} " . t('views.user_shifts.sum_report.table.shifts') ?></td>
      </tr>
<?php
    }
  }
?>
    </tbody>
  </table>
<?php
}
?>
</div>
