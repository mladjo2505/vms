<?php

if (!isset($_SESSION['user_id']) || $_SESSION['user_type'] > 1) {
  header("Location: index.php");
  die();
}

$events = Events::all();
?>
<div class="whitebg-mid">
  <table class='table table-striped table-bordered'>
    <caption>
      <h3><?php echo t('views.events.table.table_title') ?></h3>
    </caption>
    <thead>
      <tr>
        <th><b><?php echo t('views.events.table.number_sign') ?></b></th>
        <th><b><?php echo t('views.events.table.name') ?></b></th>
        <th><b><?php echo t('views.events.table.start_date') ?></b></th>
        <th><b><?php echo t('views.events.table.end_date') ?></b></th>
        <th><b><?php echo t('views.events.table.description') ?></b></th>
        <th><b><?php echo t('views.events.table.action') ?></b></th>
      </tr>
    </thead>
    <tbody>
<?php
if (empty($events)) {
?>
      <tr>
        <td class="text-center" colspan="6"><?php echo t('views.events.table.empty_message') ?></td>
      </tr>
<?php
} else {
  foreach ($events as $event) {
?>
      <tr>
        <td><?php echo $event["id"] ?></td>
        <td><?php echo $event["name"] ?></td>
        <td><?php echo $event["start_date"] ?></td>
        <td><?php echo $event["end_date"] ?></td>
        <td><?php echo $event["description"] ?></td>
        <td>
          <a class="btn btn-default btn-xs" href="index.php?content=events_destroy&id=<?php echo $event['id']; ?>"><span class="glyphicon glyphicon-trash"> <?php echo t('views.events.table.delete_button') ?></span></a>
          <a class="btn btn-default btn-xs" href="index.php?content=events_edit&id=<?php echo $event['id']; ?>"><span class="glyphicon glyphicon-edit"> <?php echo t('views.events.table.edit_button') ?></span></a>
          <a class="btn btn-default btn-xs" href="index.php?content=shifts_index&event_id=<?php echo $event['id']; ?>"><span class="glyphicon glyphicon-edit"> <?php echo t('views.events.table.show_shifts_button') ?></span></a>
        </td>
      </tr>
<?php
  }
}
?>
      <tr>
        <td class="text-left" colspan="6">
          <a class="btn btn-default btn-xs" href="index.php?content=events_new"><span class="glyphicon glyphicon-plus"> <?php echo t('views.events.table.add_new_button') ?></span></a>
        </td>
      </tr>
    </tbody>
  </table>
</div>
