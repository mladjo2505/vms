<?php

if (!isset($_SESSION['user_id']) || $_SESSION['user_type'] > 1) {
  header("Location: index.php");
  die();
}

$shifts_and_people_mismatch = false;
$created_success = false;

if (isset($_POST["submit"])) {
  $event = [];
  $event['name'] = clean($_POST["name"]);
  // TODO: Check in front-end if dates are valid before allowing submit.
  $event['start_date'] = clean($_POST["start_date"]);
  $event['end_date'] = clean($_POST["end_date"]);
  $event['description'] = clean($_POST["description"]);
  $shift_hours = clean($_POST["shift_hours"]);
  $people_needed = clean($_POST["people_needed"]);
  $shifts = Shifts::compile_shifts($shift_hours, $people_needed, $event['start_date'], $event['end_date']);
  if (!isset($shifts['count_mismatch_error'])) {
    if (Events::create_with_shifts($event, $shifts)) {
      LogEntries::create("[new_event] User '{$_SESSION['username']}' added a new event '{$event['name']}'.");
      $created_success = true;
    }
  } else {
    $shifts_and_people_mismatch = true;
  }
}
?>
<div class="container whitebg">
  <form class="form-other" role="form" method="POST" action="" onsubmit="return checkNewEventForm()">
    <h2><?php echo t('views.events.new_form.form_title') ?></h2>
    <br/>
    <input class="form-control form-control-top" type="text" required="" placeholder="<?php echo t('views.events.new_form.placeholder.name') ?>" id="name" name="name"/>
    <input class="form-control form-control-top form-control-bottom picker" type="text" required="" placeholder="<?php echo t('views.events.new_form.placeholder.start_date') ?>" id="start_date" name="start_date"/>
    <input class="form-control form-control-top form-control-bottom picker" type="text" required="" placeholder="<?php echo t('views.events.new_form.placeholder.end_date') ?>" id="end_date" name="end_date"/>
    <input class="form-control form-control-bottom" type="text" required="" placeholder="<?php echo t('views.events.new_form.placeholder.description') ?>" id="description" name="description"/><br/><br/>
    <label class="alert alert-info infobox fullwidth"><span class='glyphicon glyphicon-info-sign'></span> <?php echo t('views.events.new_form.auto_generated_shifts_notice') ?></label><br/><br/>
    <label class="fullwidth" for="shifts"><?php echo t('views.events.new_form.shifts') ?></label>
    <input class="form-control" type="text" value="<?php echo t('views.events.new_form.shift_hours_default') ?>" id="shift_hours" name="shift_hours"/>
    <br/>
    <label class="fullwidth"><?php echo t('views.events.new_form.people_needed_by_shifts') ?></label>
    <input class="form-control" type="text" value="<?php echo t('views.events.new_form.people_needed_by_shifts_default') ?>" id="people_needed" name="people_needed"/>
    <br/>
    <input class="btn btn-lg btn-primary btn-block" type="submit" id="submit" name="submit" value="<?php echo t('views.events.new_form.add_button') ?>"/>
    <br/>
    <div class="alert alert-warning">
<?php
if ($shifts_and_people_mismatch) {
  echo t('views.events.error_message.shifts_and_people_needed_count_mismatch');
} elseif ($created_success) {
    echo t('views.events.new_form.created_success');
} else {
  echo t('views.events.new_form.form_info');
}
?>
    </div>
  </form>
  <script type="text/javascript">
    $("#start_date").datepicker( { format: "yyyy-mm-dd", weekStart: 1 } );
    $("#end_date").datepicker( { format: "yyyy-mm-dd", weekStart: 1 } );
  </script>
</div>

<?php
die();
